<?php

/**
 * @file
 * Administrative callbacks and form builder functions for Commerce SOS.
 */

/**
 * Commerce SOS admin form.
 */
function commerce_sos_admin_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  // Override path for order creation.
  $form['commerce_sos_override_path_order_creation'] = [
    '#type' => 'checkbox',
    '#title' => t('Override Path for Order Creation'),
    '#description' => t('Enable if you want to replace the core order creation form with SOS\'s order creation form.'),
    '#default_value' => variable_get('commerce_sos_override_path_order_creation', 0),
  ];

  // Override path for order editing.
  $form['commerce_sos_override_path_order_editing'] = [
    '#type' => 'checkbox',
    '#title' => t('Override Path for Order Editing'),
    '#description' => t('Enable if you want to replace the core order creation form with SOS\'s order editing page.'),
    '#default_value' => variable_get('commerce_sos_override_path_order_editing', 0),
  ];

  $profile_type_options = [];
  foreach (commerce_customer_profile_type_get_name() as $type => $name) {
    $profile_type_options[$type] = $name;
  }
  // Billing address profile.
  $form['commerce_sos_billing_profile_type'] = [
    '#type' => 'select',
    '#options' => $profile_type_options,
    '#title' => t('Billing Address Profile'),
    '#description' => t('Select the billing address profile type.'),
    '#default_value' => variable_get('commerce_sos_billing_profile_type', ''),
  ];

  // Shipping address profile.
  $form['commerce_sos_shipping_profile_type'] = [
    '#type' => 'select',
    '#options' => $profile_type_options,
    '#title' => t('Shipping Address Profile'),
    '#description' => t('Select the shipping address profile type.'),
    '#default_value' => variable_get('commerce_sos_shipping_profile_type', ''),
  ];

  // Inject order ID into the cart view checkout form checkbox field.
  $form['commerce_sos_inject_order_id'] = [
    '#type' => 'checkbox',
    '#title' => t('Inject Order ID'),
    '#description' => t('Inject the order id into the views header of the cart and the the very top of all checkout forms.'),
    '#default_value' => variable_get('commerce_sos_inject_order_id', 0),
  ];

  // Select product types that should be in the product search.
  $options = [];
  foreach (commerce_product_types() as $key => $type) {
    $options[$key] = $type['name'];
  }
  $form['commerce_sos_product_types_in_search'] = [
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => $options,
    '#title' => t('Product Types Included in Product Search'),
    '#description' => t('Select the product types that should be part of the product search.'),
    '#default_value' => variable_get('commerce_sos_product_types_in_search', []),
  ];

  // Select image field to be used in product searches and order details.
  $form['field_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Select Product Image Field'),
  ];
  $options = [NULL => t('No field')];
  foreach (field_info_fields() as $field_name => $field) {
    if ($field['type'] == 'image') {
      $options[$field_name] = $field_name;
    }
  }

  $form_state['commerce_product_types'] = commerce_product_types();
  foreach ($form_state['commerce_product_types'] as $name => $type) {
    $variable_name = 'commerce_sos_image_field_' . $name;

    $form['field_settings'][$name] = [
      '#type' => 'fieldset',
      '#title' => $type['name'],
      '#states' => [
        'invisible' => [
          ':input[name="commerce_sos_available_products[' . $name . ']"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['field_settings'][$name][$variable_name] = [
      '#type' => 'select',
      '#title' => t('Image field(s)'),
      '#description' => t('Select the image field to be used in product searches and order details.'),
      '#options' => $options,
      '#default_value' => variable_get($variable_name, NULL),
      '#multiple' => TRUE,
    ];

    $variable_name = 'commerce_sos_image_default_' . $name;

    $form['field_settings'][$name][$variable_name] = [
      '#type' => 'managed_file',
      '#title' => t('Default image'),
      '#description' => t('You may provide a default image for when no other image is available.'),
      '#default_value' => variable_get($variable_name, NULL),
      '#upload_location' => 'public://commerce_sos_default_images',
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['png gif jpg jpeg'],
      ],
    ];
  }

  // Fields to be included in the Advanced Tab of the SOS Order form.
  // First exclude fields that we don't want to show.
  $fields_to_exclude = [
    'commerce_line_items',
    'commerce_order_total',
    'commerce_customer_billing',
    'commerce_coupons',
    'commerce_discounts',
    'commerce_customer_shipping',
    'field_order_comments',
  ];
  $instances = field_info_instances('commerce_order', 'commerce_order');
  $options = [];
  foreach ($instances as $field_name => $instance) {
    if (!in_array($field_name, $fields_to_exclude)) {
      $options[$field_name] = $instance['label'] . ' (' . $field_name . ')';
    }
  }

  if (!empty($options)) {
    $form['commerce_sos_order_form_advanced_fields'] = [
      '#type' => 'select',
      '#title' => t('Fields to be included in \'Advanced Tab\''),
      '#description' => t('Select fields to be included in the \'Advanced\' tab of the SOS order form.'),
      '#multiple' => TRUE,
      '#options' => $options,
      '#size' => 10,
      '#default_value' => variable_get('commerce_sos_order_form_advanced_fields', []),
    ];
  }

  // Select credit card payment service.
  if (module_exists('commerce_payment')) {
    // Otherwise present the payment method selection form.
    $event = rules_get_cache('event_commerce_payment_methods');

    // Build an options array of all available payment methods that can setup
    // transactions using the local terminal. If there is more than one instance
    // of any payment method available on site, list them in optgroups using the
    // payment method title.
    $instances = array();
    $options = array();
    $optgroups = FALSE;

    // Only build the options array if payment method Rules are enabled.
    if (!empty($event)) {
      foreach (commerce_payment_methods() as $method_id => $payment_method) {
        // Only check payment methods that should appear on the terminal.
        if ($payment_method['terminal']) {
          // Look for a Rule enabling this payment method.
          foreach ($event->getIterator() as $rule) {
            foreach ($rule->actions() as $action) {
              // If an action is found, add its instance to the options array.
              if ($action->getElementName() == 'commerce_payment_enable_' . $method_id) {
                $instances[check_plain($payment_method['title'])][] = array(
                  'instance_id' => commerce_payment_method_instance_id($method_id, $rule),
                  'label' => check_plain($rule->label()),
                );

                // If this is the second instance for this payment method, turn
                // on optgroups.
                if (count($instances[check_plain($payment_method['title'])]) > 1) {
                  $optgroups = TRUE;
                }
              }
            }
          }
        }
      }

      // Build an options array based on whether or not optgroups are necessary.
      foreach ($instances as $optgroup => $values) {
        foreach ($values as $value) {
          if ($optgroups) {
            $options[$optgroup][$value['instance_id']] = $value['label'];
          }
          else {
            $options[$value['instance_id']] = $value['label'];
          }
        }
      }

      $form['commerce_sos_credit_card_payment_service'] = [
        '#type' => 'select',
        '#title' => t('Credit card payment service'),
        '#description' => t('Select which payment service to use for making credit card transactions through the SOS payment form.'),
        '#options' => $options,
        '#default_value' => variable_get('commerce_sos_credit_card_payment_service', ''),
        '#required' => TRUE,
      ];
    }
  }

  // Advanced settings.
  $form['commerce_sos_advanced_config'] = [
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];

  $form['commerce_sos_advanced_config']['commerce_sos_search_results_count'] = [
    '#type' => 'textfield',
    '#title' => t('Search Results Count'),
    '#required' => TRUE,
    '#description' => t('Set the number of results to show in the SOS product autocomplete.'),
    '#default_value' => variable_get('commerce_sos_search_results_count', 5),
    '#element_validate' => ['element_validate_integer_positive'],
  ];

  // Configure the search api index for SOS.
  if (module_exists('search_api')) {
    $index_options = [
      'default' => t('Use default search'),
    ];
    foreach (search_api_index_load_multiple(FALSE) as $id => $index) {
      $index_options[$id] = $index->name;
    }

    $form['commerce_sos_advanced_config']['commerce_sos_search_api_index'] = [
      '#type' => 'select',
      '#title' => t('Search API Index'),
      '#description' => t('If you would like to use a search API index when searching for products in the SOS, you may select one here.'),
      '#options' => $index_options,
      '#default_value' => variable_get('commerce_sos_search_api_index', NULL),
    ];
  }

  // Select initial order status when creating new orders.
  // Build an array of order status options grouped by order state.
  $options = [];

  foreach (commerce_order_state_get_title() as $name => $title) {
    foreach (commerce_order_statuses(['state' => $name]) as $order_status) {
      $options[check_plain($title)][$order_status['name']] = check_plain($order_status['title']);
    }
  }
  $form['commerce_sos_advanced_config']['commerce_sos_initial_order_status'] = [
    '#type' => 'select',
    '#title' => t('Status for New Order'),
    '#options' => $options,
    '#description' => t('Select the status to put the order into when creating new orders.'),
    '#default_value' => variable_get('commerce_sos_initial_order_status', 'checkout_review'),
  ];

  // Select order statuses that will still allow order modification.
  $form['commerce_sos_advanced_config']['commerce_sos_order_completion_statuses'] = [
    '#type' => 'select',
    '#title' => t('Order Completion Statuses'),
    '#options' => $options,
    '#multiple' => TRUE,
    '#rows' => 4,
    '#description' => t('Select the order completion statuses that will still allow order modifications.'),
    '#default_value' => variable_get('commerce_sos_order_completion_statuses', [COMMERCE_PAYMENT_STATUS_PENDING]),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  ];

  return $form;
}

/**
 * Submit function for the admin form.
 */
function commerce_sos_admin_form_submit($form, &$form_state) {
  variable_set('commerce_sos_override_path_order_creation', $form_state['values']['commerce_sos_override_path_order_creation']);
  variable_set('commerce_sos_override_path_order_editing', $form_state['values']['commerce_sos_override_path_order_editing']);
  variable_set('commerce_sos_inject_order_id', $form_state['values']['commerce_sos_inject_order_id']);
  variable_set('commerce_sos_product_types_in_search', $form_state['values']['commerce_sos_product_types_in_search']);
  variable_set('commerce_sos_billing_profile_type', $form_state['values']['commerce_sos_billing_profile_type']);
  variable_set('commerce_sos_shipping_profile_type', $form_state['values']['commerce_sos_shipping_profile_type']);
  variable_set('commerce_sos_initial_order_status', $form_state['values']['commerce_sos_advanced_config']['commerce_sos_initial_order_status']);
  variable_set('commerce_sos_order_completion_statuses', $form_state['values']['commerce_sos_advanced_config']['commerce_sos_order_completion_statuses']);
  variable_set('commerce_sos_credit_card_payment_service', $form_state['values']['commerce_sos_credit_card_payment_service']);

  if (isset($form_state['values']['commerce_sos_order_form_advanced_fields'])) {
    variable_set('commerce_sos_order_form_advanced_fields', $form_state['values']['commerce_sos_order_form_advanced_fields']);
  }

  // Set the commerce product types image fields.
  foreach ($form_state['commerce_product_types'] as $name => $type) {
    variable_set('commerce_sos_image_field_' . $name, $form_state['values']['field_settings'][$name]['commerce_sos_image_field_' . $name]);

    $image_variable = 'commerce_sos_image_default_' . $name;
    $existing_image_fid = variable_get($image_variable, 0);
    $remove_existing = FALSE;
    $new_fid = $existing_image_fid;

    if (!empty($form_state['values']['field_settings'][$name][$image_variable])) {
      if ($form_state['values']['field_settings'][$name][$image_variable] != $existing_image_fid) {
        $file = file_load($form_state['values']['field_settings'][$name][$image_variable]);
        $file->status = FILE_STATUS_PERMANENT;

        file_save($file);
        file_usage_add($file, 'commerce_sos', 'settings', 1);

        $new_fid = $file->fid;
        $remove_existing = TRUE;
      }
    }
    else {
      $new_fid = 0;
      $remove_existing = TRUE;
    }

    if ($existing_image_fid && $remove_existing) {
      if ($file = file_load($existing_image_fid)) {
        file_delete($file, TRUE);
      }
    }

    variable_set($image_variable, $new_fid);
  }

  if (!empty($form_state['storage']['files'])) {
    foreach ($form_state['storage']['files'] as $variable_name => $file) {
      variable_set($variable_name, $file->fid);
    }
  }

  // Set the search results count.
  variable_set('commerce_sos_search_results_count', $form_state['values']['commerce_sos_advanced_config']['commerce_sos_search_results_count']);

  // Set the search api index for SOS.
  if (!empty($form_state['values']['commerce_sos_advanced_config']['commerce_sos_search_api_index'])) {
    variable_set('commerce_sos_search_api_index', $form_state['values']['commerce_sos_advanced_config']['commerce_sos_search_api_index']);
  }

  // Rebuild the menu for our override paths.
  menu_rebuild();
}
