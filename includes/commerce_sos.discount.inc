<?php

/**
 * @file
 * Contains discount related code for the commerce_sos module.
 */

/**
 * Retrieves the existing amount for a discount on a line item, if one exists.
 */
function commerce_sos_get_existing_line_item_discount_amount($line_item_id, $discount_name = COMMERCE_SOS_LINE_ITEM_DISCOUNT_NAME) {
  if ($line_item = commerce_line_item_load($line_item_id)) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    return commerce_sos_get_line_item_discount_data($line_item_wrapper, $discount_name);
  }

  return FALSE;
}

/**
 * Fetches the existing amount for a discount on a line item.
 */
function commerce_sos_get_line_item_discount_data($line_item_wrapper, $discount_name) {
  $data = [
    'type' => '',
    'amount' => 0,
  ];

  if ($component = commerce_sos_get_discount_component($line_item_wrapper->commerce_unit_price, $discount_name)) {
    $data['type'] = $component['price']['data']['sos_discount_type'];
    $data['currency_code'] = $component['price']['currency_code'];

    // Found our discount, return its amount.
    if ($component['price']['data']['sos_discount_type'] == 'percent') {
      $data['amount'] = $component['price']['data']['sos_discount_rate'] * 100;
    }
    else {
      $data['amount'] = number_format(abs($component['price']['amount'] / 100), 2);
    }
  }

  return $data;
}

/**
 * Retrieves the price component relating to SOS discount from a price field.
 *
 * @param EntityMetadataWrapper $price_wrapper
 *   A metadata wrapper around a commerce price field.
 *
 * @return array|bool
 *   The price component, or FALSE if none was found.
 */
function commerce_sos_get_discount_component(EntityMetadataWrapper $price_wrapper, $discount_name) {
  $data = (array) $price_wrapper->data->value() + ['components' => []];

  // Look for our discount in each of the price components.
  foreach ($data['components'] as $key => $component) {
    if (!empty($component['price']['data']['discount_name'])) {
      if ($component['price']['data']['discount_name'] == $discount_name) {
        return $component;
      }
    }
  }

  return FALSE;
}

/**
 * Adds a discount to a specific line item in the order.
 */
function commerce_sos_add_line_item_discount($type, $line_item_id, $amount) {
  if ($line_item = commerce_line_item_load($line_item_id)) {
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    // Remove any existing discount components on the line item.
    commerce_sos_remove_discount_components($wrapper->commerce_unit_price, COMMERCE_SOS_LINE_ITEM_DISCOUNT_NAME);
    $pre_discount_amount = $wrapper->commerce_unit_price->amount->raw();

    commerce_sos_apply_discount($wrapper, $type, $amount);

    $wrapper->commerce_unit_price->amount->set($pre_discount_amount);
    $wrapper_value = $wrapper->value();
    commerce_line_item_rebase_unit_price($wrapper_value);

    $wrapper->save();
  }
}

/**
 * Remove discount components from a given price and recalculate the total.
 *
 * @param object $price_wrapper
 *   Wrapped commerce price.
 */
function commerce_sos_remove_discount_components($price_wrapper, $discount_name_to_remove) {
  $discount_amounts = 0;

  $data = (array) $price_wrapper->data->value() + ['components' => []];
  $component_removed = FALSE;
  // Remove price components belonging to order discounts.
  foreach ($data['components'] as $key => $component) {
    $remove = FALSE;

    // Remove all discount components.
    if (!empty($component['price']['data']['discount_name'])) {
      $discount_name = $component['price']['data']['discount_name'];

      if ($discount_name_to_remove == $discount_name) {
        $remove = TRUE;
      }
    }

    if ($remove) {
      $discount_amounts += $component['price']['amount'];

      unset($data['components'][$key]);
      $component_removed = TRUE;
    }
  }
  // Don't alter the price components if no components were removed.
  if (!$component_removed) {
    return;
  }

  // Re-save the price without the discounts (if existed).
  $price_wrapper->data->set($data);

  // Re-set the total price.
  $new_total = $price_wrapper->amount->raw() - $discount_amounts;
  $price_wrapper->amount->set($new_total);
}

/**
 * Apply a specific type of discount.
 *
 * This simply services as a centralized function to control which discount
 * method(s) to call, rather than each individual piece of coding having to
 * determine where to call applyPercentDiscount or applyFixedDiscount.
 */
function commerce_sos_apply_discount($wrapper, $type, $rate) {
  switch ($type) {
    case 'percent':
      commerce_sos_apply_percent_discount($wrapper, $rate);

      break;

    case 'fixed':
      commerce_sos_apply_fixed_discount($wrapper, $rate);

      break;
  }
}

/**
 * A modified version of commerce_discount_percentage().
 */
function commerce_sos_apply_percent_discount($wrapper, $rate) {
  // Get the line item types to apply the discount to.
  $line_item_types = variable_get('commerce_discount_line_item_types', array('product' => 'product'));

  if ($rate > 1) {
    $rate = $rate / 100;
  }

  $component_data = [
    'sos_discount_type' => 'percent',
    'sos_discount_rate' => $rate,
  ];

  switch ($wrapper->type()) {
    case 'commerce_order':

      $discount_name = COMMERCE_SOS_ORDER_DISCOUNT_NAME;

      $calculated_discount = 0;
      // Loop through the product line items of the order and calculate the
      // total discount.
      foreach ($wrapper->commerce_line_items as $line_item_wrapper) {
        if (!empty($line_item_types[$line_item_wrapper->type->value()])) {
          $line_item_total = commerce_price_wrapper_value($line_item_wrapper, 'commerce_total', TRUE);
          $calculated_discount += $line_item_total['amount'] * $rate;
        }
      }

      if ($calculated_discount) {
        $discount_amount = [
          'amount' => $calculated_discount * -1,
          'currency_code' => $wrapper->commerce_order_total->currency_code->value(),
        ];

        // Modify the existing discount line item or add a new line item
        // if that fails.
        if (!commerce_sos_set_existing_line_item_price($wrapper, $discount_name, $discount_amount, $component_data)) {
          commerce_sos_discount_add_line_item($wrapper, $discount_name, $discount_amount, $component_data);
        }
      }
      else {
        commerce_sos_remove_order_discount_line_items($wrapper);
      }

      break;

    case 'commerce_line_item':

      $discount_name = COMMERCE_SOS_LINE_ITEM_DISCOUNT_NAME;

      // Check if the line item is configured in the settings to apply the
      // discount.
      if (empty($line_item_types[$wrapper->getBundle()])) {
        return;
      }

      $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price', TRUE);
      $calculated_discount = $unit_price['amount'] * $rate * -1;

      if ($calculated_discount) {
        $discount_amount = [
          'amount' => $calculated_discount,
          'currency_code' => $unit_price['currency_code'],
        ];

        commerce_sos_add_price_component($wrapper, $discount_name, $discount_amount, $component_data);
      }
      break;
  }
}

/**
 * A modified version of commerce_discount_fixed_amount().
 */
function commerce_sos_apply_fixed_discount(EntityMetadataWrapper $wrapper, $discount_amount) {
  $discount_price['amount'] = -$discount_amount;
  $line_item_types = variable_get('commerce_discount_line_item_types', array('product' => 'product'));

  $component_data = [
    'sos_discount_type' => 'fixed',
  ];

  switch ($wrapper->type()) {
    case 'commerce_order':

      $discount_name = COMMERCE_SOS_ORDER_DISCOUNT_NAME;

      if ($discount_amount) {
        $discount_price['currency_code'] = $wrapper->commerce_order_total->currency_code->value();

        // If the discount will bring the order to less than zero, set the
        // discount amount so that it stops at zero.
        // Loop the line items of the order and calculate the total discount.
        $order_amount = 0;

        foreach ($wrapper->commerce_line_items as $line_item_wrapper) {
          if (!empty($line_item_types[$line_item_wrapper->type->value()])) {
            $line_item_total = commerce_price_wrapper_value($line_item_wrapper, 'commerce_total', TRUE);
            $order_amount += $line_item_total['amount'];
          }
        }

        if (-$discount_price['amount'] > $order_amount) {
          $discount_price['amount'] = -$order_amount;
        }

        // Modify the existing discount line item or add a new one if that
        // fails.
        if (!commerce_sos_set_existing_line_item_price($wrapper, $discount_name, $discount_price, $component_data)) {
          commerce_sos_discount_add_line_item($wrapper, $discount_name, $discount_price, $component_data);
        }
      }
      else {
        commerce_sos_remove_order_discount_line_items($wrapper);
      }

      break;

    case 'commerce_line_item':

      $discount_name = COMMERCE_SOS_LINE_ITEM_DISCOUNT_NAME;

      if ($discount_amount) {

        // Do not allow negative line item totals.
        $line_item_amount = $wrapper->commerce_unit_price->amount->value();
        if (-$discount_price['amount'] > $line_item_amount) {
          $discount_price['amount'] = -$line_item_amount;
        }

        $discount_price['currency_code'] = $wrapper->commerce_unit_price->currency_code->value();

        commerce_sos_add_price_component($wrapper, $discount_name, $discount_price, $component_data);
      }

      break;
  }
}

/**
 * Updates the unit price of an existing discount line item.
 *
 * Non-discount line items are ignored.
 *
 * @param EntityDrupalWrapper $order_wrapper
 *   The wrapped order entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 * @param array $discount_price
 *   The discount amount price array (amount, currency_code).
 * @param array $component_data
 *   Any price data to merge into the component.
 *
 * @return bool
 *   TRUE if an existing line item was successfully modified, FALSE otherwise.
 */
function commerce_sos_set_existing_line_item_price(EntityDrupalWrapper $order_wrapper, $discount_name, array $discount_price, array $component_data = []) {
  $modified_existing = FALSE;
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->getBundle() == 'commerce_sos_discount') {
      // Add the discount component price if the line item was originally
      // added by discount module.
      $line_item = $line_item_wrapper->value();
      if (isset($line_item->data['discount_name']) && $line_item->data['discount_name'] == $discount_name) {
        commerce_sos_set_price_component($line_item_wrapper, $discount_name, $discount_price, $component_data);
        $modified_existing = TRUE;
        $line_item_wrapper->save();
      }
    }
  }

  return $modified_existing;
}

/**
 * Sets a discount price component to the provided line item.
 *
 * @param EntityDrupalWrapper $line_item_wrapper
 *   The wrapped line item entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 * @param array $discount_amount
 *   The discount amount price array (amount, currency_code).
 * @param array $component_data
 *   Any price data to merge into the component.
 */
function commerce_sos_set_price_component(EntityDrupalWrapper $line_item_wrapper, $discount_name, array $discount_amount, array $component_data = []) {
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  // Currencies don't match, abort.
  if ($discount_amount['currency_code'] != $unit_price['currency_code']) {
    return;
  }

  $discount_amount['data'] = [
    'discount_name' => $discount_name,
    'sos_discount_component_title' => commerce_sos_get_discount_component_title($discount_name),
  ];

  $discount_amount['data'] += $component_data;

  // Set the new unit price.
  $line_item_wrapper->commerce_unit_price->amount = $discount_amount['amount'];
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_delete($line_item_wrapper->commerce_unit_price->value(), 'discount|sos_order_discount');

  // Add the discount amount as a price component.
  $price = $line_item_wrapper->commerce_unit_price->value();
  $type = check_plain('discount|' . $discount_name);
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($price, $type, $discount_amount, TRUE, TRUE);

  commerce_sos_calculate_taxes($line_item_wrapper);

  // Update the line item total.
  commerce_sos_update_line_item_total($line_item_wrapper);
}

/**
 * Creates a discount line item on the provided order.
 *
 * @param EntityDrupalWrapper $order_wrapper
 *   The wrapped order entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 * @param array $discount_amount
 *   The discount amount price array (amount, currency_code).
 * @param array $data
 *   Any additional data to be added to the price component.
 */
function commerce_sos_discount_add_line_item(EntityDrupalWrapper $order_wrapper, $discount_name, array $discount_amount, array $data) {
  // Create a new line item.
  $values = [
    'type' => 'commerce_sos_discount',
    'order_id' => $order_wrapper->order_id->value(),
    'quantity' => 1,
    // Flag the line item.
    'data' => ['discount_name' => $discount_name],
  ];
  $discount_line_item = entity_create('commerce_line_item', $values);
  $discount_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $discount_line_item);

  // Initialize the line item unit price.
  $discount_line_item_wrapper->commerce_unit_price->amount = 0;
  $discount_line_item_wrapper->commerce_unit_price->currency_code = $discount_amount['currency_code'];

  // Reset the data array of the line item total field to only include a
  // base price component, set the currency code from the order.
  $base_price = [
    'amount' => 0,
    'currency_code' => $discount_amount['currency_code'],
    'data' => [],
  ];

  $discount_line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($base_price, 'base_price', $base_price, TRUE);

  // Add the discount price component.
  commerce_sos_add_price_component($discount_line_item_wrapper, $discount_name, $discount_amount, $data);

  commerce_sos_calculate_taxes($discount_line_item_wrapper);

  // Save the line item and add it to the order.
  $discount_line_item_wrapper->save();
  $order_wrapper->commerce_line_items[] = $discount_line_item_wrapper;
}

/**
 * Adds a discount price component to the provided line item.
 *
 * @param EntityDrupalWrapper $line_item_wrapper
 *   The wrapped line item entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 * @param array $discount_amount
 *   The discount amount price array (amount, currency_code).
 * @param array $data
 *   Any additional data to be merged into the new price component's data
 *   array.
 */
function commerce_sos_add_price_component(EntityDrupalWrapper $line_item_wrapper, $discount_name, array $discount_amount, array $data) {
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  $current_amount = $unit_price['amount'];
  // Currencies don't match, abort.
  if ($discount_amount['currency_code'] != $unit_price['currency_code']) {
    return;
  }

  // Calculate the updated amount and create a price array representing the
  // difference between it and the current amount.
  $updated_amount = commerce_round(COMMERCE_ROUND_HALF_UP, $current_amount + $discount_amount['amount']);

  $difference = [
    'amount' => commerce_round(COMMERCE_ROUND_HALF_UP, $discount_amount['amount']),
    'currency_code' => $discount_amount['currency_code'],
    'data' => [
      'discount_name' => $discount_name,
      'sos_discount_component_title' => commerce_sos_get_discount_component_title($discount_name),
    ],
  ];

  $difference['data'] += $data;

  // Set the new unit price.
  $line_item_wrapper->commerce_unit_price->amount = $updated_amount;

  // Add the discount amount as a price component.
  $price = $line_item_wrapper->commerce_unit_price->value();
  $type = check_plain('discount|' . $discount_name);
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($price, $type, $difference, TRUE, TRUE);

  // Update the line item total.
  commerce_sos_update_line_item_total($line_item_wrapper);
}

/**
 * Retrieves a display name for a specific discount type.
 */
function commerce_sos_get_discount_component_title($discount_name) {
  switch ($discount_name) {
    case COMMERCE_SOS_LINE_ITEM_DISCOUNT_NAME:
      return t('Product Discount');

    case COMMERCE_SOS_ORDER_DISCOUNT_NAME:
      return t('Order Discount');

    default:
      return FALSE;
  }
}

/**
 * Update commerce_total without saving line item.
 *
 * To have the order total refreshed without saving the line item.
 * Taken from CommerceLineItemEntityController::save().
 */
function commerce_sos_update_line_item_total($line_item_wrapper) {
  $quantity = $line_item_wrapper->quantity->value();

  // Update the total of the line item based on the quantity and unit price.
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);

  $line_item_wrapper->commerce_total->amount = $quantity * $unit_price['amount'];
  $line_item_wrapper->commerce_total->currency_code = $unit_price['currency_code'];

  // Add the components multiplied by the quantity to the data array.
  if (empty($unit_price['data']['components'])) {
    $unit_price['data']['components'] = [];
  }
  else {
    foreach ($unit_price['data']['components'] as $key => &$component) {
      $component['price']['amount'] *= $quantity;
    }
  }

  // Set the updated data array to the total price.
  $line_item_wrapper->commerce_total->data = $unit_price['data'];

  // Reset the cache because we aren't saving it.
  entity_get_controller('commerce_line_item')->resetCache([$line_item_wrapper->getIdentifier()]);
}

/**
 * Calculate the taxes on a given line item.
 *
 * @param EntityMetadataWrapper $line_item_wrapper
 *   A metadata wrapper representing the line item.
 */
function commerce_sos_calculate_taxes(EntityMetadataWrapper $line_item_wrapper) {
  if (module_exists('commerce_tax')) {
    module_load_include('inc', 'commerce_tax', 'commerce_tax.rules');

    // First remove all existing tax components from the line item if any
    // exist.
    commerce_tax_remove_taxes($line_item_wrapper, FALSE, array_keys(commerce_tax_rates()));

    foreach (commerce_tax_types() as $name => $type) {
      commerce_tax_calculate_by_type($line_item_wrapper->value(), $name);
    }
  }
}

/**
 * Removes all SOS discount line items from an order.
 *
 * @param EntityMetadataWrapper $order_wrapper
 *   The order entity.
 */
function commerce_sos_remove_order_discount_line_items(EntityMetadataWrapper $order_wrapper) {
  $line_items_to_delete = [];

  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'commerce_sos_discount') {
      $order_wrapper->commerce_line_items->offsetUnset($delta);
      $line_items_to_delete[] = $line_item_wrapper->line_item_id;
    }
  }

  if ($line_items_to_delete) {
    commerce_line_item_delete_multiple($line_items_to_delete);
  }
}

/**
 * Adds a discount to the order.
 *
 * @param object $order
 *   The order entity.
 * @param string $type
 *   The discount type.
 * @param int $amount
 *   The discount amount.
 */
function commerce_sos_add_order_discount($order, $type, $amount) {
  if ($wrapper = entity_metadata_wrapper('commerce_order', $order)) {
    // Apply our new discount.
    commerce_sos_apply_discount($wrapper, $type, $amount);
    $wrapper->save();
  }
}

/**
 * Adds a coupon to the order.
 *
 * @param object $order
 *   The order entity.
 * @param string $coupon_code
 *   The coupon code.
 */
function commerce_sos_add_order_coupon($order, $coupon_code) {
  $error = '';
  $coupon = commerce_coupon_redeem_coupon_code($coupon_code, $order, $error);
  $order = commerce_order_load($order->order_id);

  // Error found during redeem.
  if (!empty($error)) {
    watchdog('commerce_coupon', 'An error occurred redeeming a coupon: @error', ['@error' => $error], WATCHDOG_ERROR);
    drupal_set_message(t('Unable to redeem coupon.'), 'error');
    commerce_coupon_remove_coupon_from_order($order, $coupon);
  }

  if ($coupon) {
    // Allow modules/rules to act when a coupon has been successfully added
    // to the cart.
    rules_invoke_all('commerce_coupon_applied_to_cart', $coupon, $order);
    commerce_cart_order_refresh($order);
  }
}

/**
 * Removes a coupon from an order.
 *
 * @param object $order
 *   The order entity.
 * @param string $coupon_id
 *   The coupon id.
 */
function commerce_sos_remove_order_coupon($order, $coupon_id) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Remove the coupons from the order relationship.
  foreach ($order_wrapper->commerce_coupons as $delta => $coupon_wrapper) {
    if ($coupon_wrapper->coupon_id->value() == $coupon_id) {
      $order_wrapper->commerce_coupons->offsetUnset($delta);
    }
  }

  commerce_order_save($order);
  commerce_cart_order_refresh($order);
}



/**
 * Removes the discount coupons from an order if empty line items.
 *
 * @param EntityMetadataWrapper $order_wrapper
 *   The order wrapper.
 */
function commerce_sos_remove_discount_coupons(EntityMetadataWrapper $order_wrapper) {
  foreach ($order_wrapper->commerce_coupons as $coupon) {
    commerce_coupon_remove_coupon_from_order($order_wrapper->value(), $coupon->value(), $save = TRUE);
  }
}

/**
 * Displays the discounts in a coupon.
 *
 * @param object $order
 *   The order entity.
 * @param object $coupon
 *   The coupon entity.
 *
 * @return string
 *   An html list of discounts in the coupon.
 */
function commerce_sos_display_coupon_discount_value($order, $coupon) {
  $discounts = commerce_sos_coupon_order_coupon_code_discounts($coupon->code, $order);

  $output = '';
  // Make sure that if the coupon grants free shipping, those discounts gets
  // added too.
  $free_shipping_discount = _commerce_coupon_free_shipping_single_discount($coupon, FALSE);
  if ($free_shipping_discount) {
    $discounts[] = $free_shipping_discount;
  }

  if ($discounts) {
    $items = [];

    // Coupon codes may be linked to multiple discounts, so we have a hook
    // that lets discount type/offer modules define what value gets shown
    // for each coupon that has a discount.
    foreach ($discounts as $discount) {
      $display = '';
      drupal_alter('commerce_coupon_discount_value_display', $display, $discount, $order);
      if ($display) {
        $items[] = $display;
      }
    }
    $variables = [
      'items' => $items,
      'type' => 'ul',
      'title' => '',
      'attributes' => [],
    ];

    $output = theme_item_list($variables);
  }

  return $output;
}

/**
 * Load common discounts that are present in a coupon (by code) and an order.
 *
 * @param string $code
 *   A coupon code.
 * @param object $order
 *   An order entity.
 *
 * @return array
 *   A list of discount entities.
 *
 * @see commerce_coupon_order_coupon_code_discounts().
 */
function commerce_sos_coupon_order_coupon_code_discounts($code, $order) {
  $coupon = commerce_coupon_load_by_code($code);
  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);

  if ($coupon->type == 'discount_coupon' && $coupon_wrapper->commerce_discount_reference->value()) {

    // Line item level discounts are not stored on the order, so we have to dig
    // through the price components.
    $order_discount_ids = commerce_sos_coupon_order_discount_ids($order);
    $discount_ids = array_intersect($coupon_wrapper->commerce_discount_reference->raw(), $order_discount_ids);
    return commerce_coupon_discount_load_multiple($discount_ids);
  }

  return array();
}

/**
 * Load all discounts connected to an order.
 *
 * This includes line item level discounts traced through line item unit price
 * components.
 *
 * @param object $order
 *   An order entity.
 *
 * @return array
 *   A list of discount ids found on the order.
 *
 * @see commerce_coupon_order_discount_ids().
 */
function commerce_sos_coupon_order_discount_ids($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_discount_ids = array();

  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->value()) {
      $data = $line_item_wrapper->commerce_unit_price->data->value();

      foreach ($data['components'] as $key => $component) {
        // Make sure we're not pulling out our custom sos line item and order
        // discounts.
        if (!empty($component['price']['data']['discount_name'])
          && $component['price']['data']['discount_name'] != 'sos_line_item_discount'
          && $component['price']['data']['discount_name'] != 'sos_order_discount') {
          $order_discount_name = $component['price']['data']['discount_name'];
          $order_discount_wrapper = entity_metadata_wrapper('commerce_discount', $order_discount_name);
          // Make a list of discounts present via the order's line item price
          // components.
          $order_discount_ids[] = $order_discount_wrapper->discount_id->value();
        }
      }
    }
  }

  // Add the set of discounts directly referenced on the order.
  foreach ($order_wrapper->commerce_discounts->raw() as $discount_id) {
    $order_discount_ids[] = $discount_id;
  }

  $order_discount_ids = array_unique($order_discount_ids);

  return $order_discount_ids;
}

/**
 * Update the order discounts if already applied.
 *
 * @param object $order
 *   The order entity.
 */
function commerce_sos_discount_update_order_discounts($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->getBundle() == 'commerce_sos_discount') {
      $price_wrapper = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price');

      foreach ($price_wrapper['data']['components'] as $price_component) {
        if (isset($price_component['price']['data']['sos_discount_type'])) {
          commerce_sos_apply_discount($order_wrapper, $price_component['price']['data']['sos_discount_type'], $price_component['price']['data']['sos_discount_rate']);
        }
      }
    }
  }
}

/**
 * Removes order discounts already applied after a line item has been removed.
 *
 * @param object $order
 *   The order entity.
 */
function commerce_sos_after_delete_line_item($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  if (count($order_wrapper->commerce_line_items) == 1) {
    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      if ($line_item_wrapper->type->value() == 'commerce_pos_discount') {
        commerce_line_item_delete($line_item_wrapper->line_item_id);
        $order_wrapper->commerce_line_items->offsetUnset($delta);
      }
    }
  }
}
