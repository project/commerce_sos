<?php

/**
 * @file
 * Contains re-useable code for the commerce_sos module.
 */

/**
 * Find all profiles of a customer given an email or name or street address.
 *
 * @param string $email
 *   The email address.
 * @param string $name
 *   The name of the customer.
 * @param string $address
 *   The street address of the customer.
 * @param bool $new_customer_search
 *   True if we're performing this search for the new customer form.
 *
 * @return array
 *   An array of users keyed by the uid.
 */
function commerce_sos_find_existing_customer($email = NULL, $name = NULL, $address = NULL, $new_customer_search = FALSE) {
  $results = [];

  $query = db_select('users', 'u');
  $query = $query->extend('PagerDefault')->limit(5);
  $query = $query->fields('u', ['uid', 'mail']);
  $query->fields('f', [
    'commerce_customer_address_first_name',
    'commerce_customer_address_last_name',
    'commerce_customer_address_administrative_area',
    'commerce_customer_address_thoroughfare',
    'commerce_customer_address_locality',
    'commerce_customer_address_postal_code',
    'commerce_customer_address_country',
    'commerce_customer_address_sub_administrative_area',
    'commerce_customer_address_dependent_locality',
    'commerce_customer_address_sub_premise',
    'entity_id',
    'bundle',
  ]);
  $query->addExpression('MAX(CASE WHEN c.type = \'' . variable_get('commerce_sos_billing_profile_type', '') . '\' THEN c.profile_id ELSE 0 END)', variable_get('commerce_sos_billing_profile_type', ''));
  $query->addExpression('MAX(CASE WHEN c.type = \'' . variable_get('commerce_sos_shipping_profile_type', '') . '\' THEN c.profile_id ELSE 0 END)', variable_get('commerce_sos_shipping_profile_type', ''));
  $query->leftJoin('commerce_addressbook_defaults', 'c', 'c.uid = u.uid');
  $query->leftJoin('field_data_commerce_customer_address', 'f', 'f.entity_id = c.profile_id');
  $or = db_or();
  if (!empty($email)) {
    // If it's for the new customer form, we'll do an exact match.
    if ($new_customer_search) {
      $or->condition('u.mail', $email);
    }
    else {
      $or->condition('u.mail', '%' . db_like($email) . '%', 'LIKE');
    }
  }
  if (!empty($name)) {
    $or->condition('f.commerce_customer_address_name_line', '%' . db_like($name) . '%', 'LIKE');
  }
  if (!empty($address)) {
    $or->condition('f.commerce_customer_address_thoroughfare', '%' . db_like($address) . '%', 'LIKE');
  }
  $query->condition($or);
  $query->condition('c.uid', 0, '!=');
  $query->orderBy('c.profile_id', 'DESC');
  $query->groupBy('u.uid');
  $query->condition('c.type', [
    variable_get('commerce_sos_billing_profile_type', ''),
    variable_get('commerce_sos_shipping_profile_type'),
  ], 'IN');
  $results = $query->execute()->fetchAllAssoc('uid');

  return $results;
}

/**
 * Function to clean up username.
 *
 * E.g.
 *     Replace two or more spaces with a single underscore
 *     Strip illegal characters.
 *
 * @param string $name
 *   The username to be cleaned up.
 *
 * @return string
 *   Cleaned up username.
 *
 * @see user_registration_cleanup_username().
 */
function commerce_sos_cleanup_username($name, $uid = NULL) {
  // Strip illegal characters.
  $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', $name);

  // Strip leading and trailing spaces.
  $name = trim($name);

  // Convert any other series of spaces to a single underscore.
  $name = preg_replace('/\s+/', '_', $name);

  // If there's nothing left use a default.
  $name = ('' === $name) ? t('user') : $name;

  if (!empty($uid)) {
    // Put uid on the end of the name.
    $name = $name . '_' . $uid;
  }

  // Truncate to a reasonable size.
  $name = (drupal_strlen($name) > (USERNAME_MAX_LENGTH - 10)) ? drupal_substr($name, 0, USERNAME_MAX_LENGTH - 11) : $name;

  return $name;
}

/**
 * Given a starting point returns a legal, unique Drupal username.
 *
 * This function is designed to work on the results of the /user/register or
 * /admin/people/create forms which have already called user_validate_name,
 * valid_email_address or a similar function. If your custom code is creating
 * users, you should ensure that the email/name is already validated using
 * something like that. Alternatively you can prepend the account user name
 * to contain 'email_registration_' to allow for the email registration hooks
 * to generate a unique username based on mail.
 *
 * @param string $name
 *   A name from which to base the final user name.  May contain illegal
 *   characters; these will be stripped.
 * @param int $uid
 *   (optional) Uid to ignore when searching for unique user
 *   (e.g. if we update the username after the {users} row is inserted)
 *
 * @return string
 *   A unique user name based on $name.
 *
 * @see user_registration_unique_username().
 */
function commerce_sos_unique_username($name, $uid = NULL) {
  // Iterate until we find a unique name.
  $i = 0;
  do {
    $new_name = empty($i) ? $name : $name . '_' . $i;
    if ($uid) {
      $found = db_query_range("SELECT uid from {users} WHERE uid <> :uid AND name = :name", 0, 1, [
        ':uid' => $uid,
        ':name' => $new_name,
      ])->fetchAssoc();
    }
    else {
      $found = db_query_range("SELECT uid from {users} WHERE name = :name", 0, 1, [':name' => $new_name])->fetchAssoc();
    }

    $i++;
  } while (!empty($found));

  // Allow users to alter the new unique name.
  drupal_alter('commerce_sos_unique_username', $name, $new_name);

  return $new_name;
}

/**
 * Creates and saves a profile based on the billing/shipping profile provided.
 *
 * @param object $customer_profile
 *   The customer billing profile information.
 * @param string $type
 *   The type of profile (shipping/billing).
 *
 * @return object
 *   The newly saved shipping profile object.
 */
function commerce_sos_save_profile($customer_profile, $type) {
  $profile = new stdClass();
  $profile->type = $type;
  $profile->uid = $customer_profile->uid;
  $profile->status = 1;

  // Address field.
  $address_array = [
    'name_line',
    'first_name',
    'last_name',
    'organisation_name',
    'country',
    'administrative_area',
    'sub_administrative_area',
    'locality',
    'dependent_locality',
    'postal_code',
    'thoroughfare',
    'premise',
    'sub_premise',
  ];
  foreach ($address_array as $address_element) {
    $profile->commerce_customer_address[LANGUAGE_NONE][0][$address_element] = $customer_profile->commerce_customer_address[LANGUAGE_NONE][0][$address_element];
  }

  commerce_customer_profile_save($profile);

  return $profile;
}

/**
 * Outputs html with the address details for a billing/shipping profile.
 *
 * @param object $order
 *   The order entity we're displaying this address for.
 * @param array $address_info
 *   The billing/shipping profile along with the user details.
 * @param bool $edit_order
 *   True if we're in the SOS order editing page.
 *
 * @return string
 *   The html output.
 */
function commerce_sos_print_address_details($order, array $address_info, $edit_order = FALSE) {
  $output = '';

  if (isset($address_info['profile']->type)) {
    $output .= '<div class="sos-address-header">' . t('@type Address', ['@type' => $address_info['profile']->type]) . '</div>';
    $output .= '<div class="sos-address-email">' . $address_info['customer']->mail . '</div>';
    $output .= '<div class="sos-address-details">';
    $address_field = field_view_field('commerce_customer_profile', $address_info['profile'], 'commerce_customer_address', ['label' => 'hidden']);
    $output .= drupal_render($address_field);
    $output .= '</div>';

    // Show the edit link only if we're editing an order.
    if ($edit_order) {
      ctools_include('modal');
      ctools_modal_add_js();
      $output .= '<div class="sos-address-edit">' . l(t('Edit @type address', ['@type' => $address_info['profile']->type]), 'admin/commerce/sos/orders/' . $order->order_id . '/edit-customer-profile/nojs/' . $address_info['profile']->profile_id, [
        'attributes' => ['class' => ['ctools-use-modal']],
      ]) . '</div>';
    }
  }

  // Allow users to alter the address output.
  drupal_alter('commerce_sos_print_address_details', $address_info, $edit_order, $output);

  return $output;
}

/**
 * Saves a commerce order with the user id, status, and bill/ship profiles.
 *
 * @param object $order
 *   The order entity.
 * @param object $customer
 *   The user account.
 * @param string $status
 *   The status to set the order to.
 * @param array $profiles
 *   The billing and shipping profiles.
 *
 * @return object
 *   The newly saved order object.
 */
function commerce_sos_save_commerce_order($order, $customer = NULL, $status = 'pending', array $profiles = NULL) {
  global $user;

  if ($customer) {
    // If the logged in user is NOT the same as the customer, set the uid of the
    // user in the order. We don't set it if they are the same as there's some
    // weird bug happening with discounts not applying when they are the same.
    // Our rules will automatically set the uid of the order from the customer's
    // email when the order has officially completed checkout.
    if ($user->uid != $customer->uid) {
      $order->uid = $customer->uid;
    }
    $order->mail = $customer->mail;
    $order->name = $customer->name;
  }
  $order->status = $status;
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  if ($profiles) {
    $order_wrapper->commerce_customer_billing->set($profiles['billing_information']);
    $order_wrapper->commerce_customer_shipping->set($profiles['shipping_information']);
  }

  commerce_order_save($order);

  return $order;
}

/**
 * Callback for the product autocomplete.
 */
function commerce_sos_product_autocomplete() {
  $params = drupal_get_query_parameters();
  $products = [];

  if (!empty($params['term'])) {
    $results = commerce_sos_product_search($params['term']);

    // Let's return an empty results message back when we get no products.
    if (empty($results)) {
      $products[''] = commerce_sos_product_get_empty_results_message();
    }
    // Else, if we have results, add the themed markup to the products array.
    else {
      foreach ($results as $product_id) {
        if ($data = commerce_sos_product_autocomplete_build($product_id, TRUE)) {
          $products[$product_id] = $data;
        }
      }
    }
  }

  drupal_json_output($products);
}

/**
 * Searches for products via a keyword search.
 */
function commerce_sos_product_search($keywords) {
  $results = NULL;

  // First try and perform a search through the Search API.
  if (!($results = commerce_sos_product_search_api_search($keywords))) {
    $results = commerce_sos_product_search_basic($keywords);
  }

  return $results;
}

/**
 * Uses the Search API to perform a product keyword search.
 */
function commerce_sos_product_search_api_search($keywords) {
  $results = [];

  // Check if an index has been selected.
  if ($index_id = variable_get('commerce_sos_search_api_index')) {
    if ($index_id != 'default') {
      $index = search_api_index_load($index_id);
      $query = new SearchApiQuery($index);
      $query->condition('status', 1);
      if (!empty(variable_get('commerce_sos_product_types_in_search', []))) {
        $query->condition('type', variable_get('commerce_sos_product_types_in_search', []), 'IN');
      }
      $query->keys($keywords);
      $query->range(0, variable_get('commerce_sos_search_results_count', 5));
      $query_results = $query->execute();

      if (!empty($query_results['results'])) {
        $results = array_keys($query_results['results']);
      }
    }
  }

  return $results;
}

/**
 * Perform a very basic product search via the database.
 */
function commerce_sos_product_search_basic($keywords) {
  $results = [];

  $query = db_select('commerce_product', 'cp');
  $query->fields('cp', ['product_id', 'title', 'type']);
  $query->condition("status", 1);
  if (!empty(variable_get('commerce_sos_product_types_in_search', []))) {
    $query->condition('type', variable_get('commerce_sos_product_types_in_search', []), 'IN');
  }
  $or = db_or();
  $or->condition('title', '%' . db_like($keywords) . '%', 'LIKE')
    ->condition('sku', '%' . db_like($keywords) . '%', 'LIKE');
  $query->condition($or);
  $query->range(0, variable_get('commerce_sos_search_results_count', 5))
    ->orderBy('title', 'ASC');
  $result = $query->execute();

  foreach ($result as $row) {
    $results[] = $row->product_id;
  }

  return $results;
}

/**
 * Returns a 'No results' message when autocomplete search yields no results.
 *
 * @return array
 *   Results message markup.
 */
function commerce_sos_product_get_empty_results_message() {
  return [
    'markup' => '<div id="commerce-sos-product-no-results-message">'
    . t('There are no products that match this search.')
    . '</div>',
    'title' => t('No Results'),
  ];
}

/**
 * Adds the specified product to transaction order.
 *
 * @param object $order
 *   The order entity.
 * @param array $product_info
 *   An array of product info including the display_path and product entity.
 * @param int $quantity
 *   The number of quantity to add.
 * @param bool $combine
 *   Whether to combine similar products.
 * @param float $price
 *   The price per item.
 *
 * @return object
 *   The full saved and referenced line item.
 */
function commerce_sos_add_product($order, array $product_info, $quantity = 1, $combine = TRUE, $price = NULL) {
  // If the specified product exists...
  // Create a new product line item for it.
  $product = $product_info['product'];
  $view_url = $product_info['view_url'];
  $line_item = commerce_product_line_item_new($product, $quantity, $order->order_id);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Add the necessary data to the line item if we've got a display nid.
  if (!empty($product_info['display_nid'])) {
    $line_item->data['context'] = [
      'product_ids' => 'entity',
      'add_to_cart_combine' => $combine,
      'show_single_product_attributes' => TRUE,
      'display_path' => $view_url,
      'entity' => [
        'entity_type' => $product_info['entity_type'],
        'entity_id' => $product_info['display_nid'],
        'product_reference_field_name' => $product_info['product_reference_field_name'],
      ],
    ];
  }

  rules_invoke_event('commerce_product_calculate_sell_price', $line_item);

  if (!empty($price)) {
    $amount = $price;
    $currency = $line_item_wrapper->commerce_unit_price->currency_code->raw();

    // We "snapshot" the calculated sell price and use it as the line item's
    // base price.
    $unit_price = [
      'amount' => $amount,
      'currency_code' => $currency,
    ];

    $unit_price['data'] = commerce_price_component_add($unit_price, 'base_price', [
      'amount' => $amount,
      'currency_code' => $currency,
      'data' => [],
    ], TRUE, FALSE);

    $line_item_wrapper->commerce_unit_price->set($unit_price);
  }

  if (module_exists('commerce_pricing_attributes')) {
    // Hack to prevent the combine logic in addLineItem() from incorrectly
    // thinking that the newly-added line item is different than
    // previously-added line items.
    $line_item->commerce_pricing_attributes = serialize([]);
  }

  if (module_exists('commerce_tax')) {
    foreach (commerce_tax_types() as $name => $type) {
      commerce_tax_calculate_by_type($line_item, $name);
    }
  }

  // Set the display path for this product.
  if (!empty($view_url)) {
    // Let others alter the display path.
    drupal_alter('commerce_sos_line_item_display_path', $line_item, $product, $view_url);

    $line_item_wrapper->commerce_display_path->set($view_url);
  }

  return commerce_sos_add_line_item($order, $line_item, $combine);
}

/**
 * Saves a line item and references the line item to an order.
 *
 * @param object $order
 *   The order entity.
 * @param object $line_item
 *   The line item entity.
 * @param bool $combine
 *   Whether to combine the line items or not.
 *
 * @return object
 *   Returns the full saved line item.
 */
function commerce_sos_add_line_item($order, $line_item, $combine) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Set the incoming line item's order_id.
  $line_item->order_id = $order->order_id;

  // Wrap the order for easy access to field data.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Extract the product and quantity we're adding from the incoming line item.
  $product = $line_item_wrapper->commerce_product->value();
  $quantity = $line_item->quantity;

  // Invoke the product prepare event with the shopping cart order.
  rules_invoke_all('commerce_cart_product_prepare', $order, $product, $line_item->quantity);

  // Determine if the product already exists on the order and increment its
  // quantity instead of adding a new line if it does.
  $matching_line_item = NULL;

  // If we are supposed to look for a line item to combine into...
  if ($combine) {
    // Generate an array of properties and fields to compare.
    $comparison_properties = ['type', 'commerce_product'];

    // Add any field that was exposed on the Add to Cart form to the array.
    foreach (field_info_instances('commerce_line_item', $line_item->type) as $info) {
      if (!empty($info['commerce_cart_settings']['field_access'])) {
        $comparison_properties[] = $info['field_name'];
      }
    }

    // Allow other modules to specify what properties should be compared when
    // determining whether or not to combine line items.
    $cloned_line_item = clone($line_item);
    drupal_alter('commerce_cart_product_comparison_properties', $comparison_properties, $cloned_line_item);

    // Loop over each line item on the order.
    foreach ($order_wrapper->commerce_line_items as $delta => $matching_line_item_wrapper) {
      // Examine each of the comparison properties on the line item.
      foreach ($comparison_properties as $property) {
        // If the property is not present on either line item, bypass it.
        if (!isset($matching_line_item_wrapper->value()->{$property}) && !isset($line_item_wrapper->value()->{$property})) {
          continue;
        }

        // If any property does not match the same property on the incoming line
        // item or exists on one line item but not the other...
        if ((!isset($matching_line_item_wrapper->value()->{$property}) && isset($line_item_wrapper->value()->{$property})) ||
          (isset($matching_line_item_wrapper->value()->{$property}) && !isset($line_item_wrapper->value()->{$property})) ||
          $matching_line_item_wrapper->{$property}->raw() != $line_item_wrapper->{$property}->raw()
        ) {
          // Continue the loop with the next line item.
          continue 2;
        }
      }

      // If every comparison line item matched, combine into this line item.
      $matching_line_item = $matching_line_item_wrapper->value();
      break;
    }
  }

  // If no matching line item was found...
  if (empty($matching_line_item)) {
    // Save the incoming line item now so we get its ID.
    commerce_line_item_save($line_item);

    // Add it to the order's line item reference value.
    $order_wrapper->commerce_line_items[] = $line_item;
  }
  else {
    // Increment the quantity of the matching line item, update the data array,
    // and save it.
    $matching_line_item->quantity += $quantity;
    $matching_line_item->data = array_merge($line_item->data, $matching_line_item->data);

    commerce_line_item_save($matching_line_item);

    // Clear the line item cache so the updated quantity will be available to
    // the ensuing load instead of the original quantity as loaded above.
    entity_get_controller('commerce_line_item')->resetCache([$matching_line_item->line_item_id]);

    // Update the line item variable for use in the invocation and return value.
    $line_item = $matching_line_item;
  }

  // Save the updated order.
  commerce_order_save($order);

  // Return the line item.
  commerce_sos_commerce_line_item_updated($order);
  return $line_item;
}

/**
 * Removes a line item from an order.
 *
 * @param object $order
 *   The order entity.
 * @param int $line_item_id
 *   The line item id.
 *
 * @throws \Exception
 */
function commerce_sos_remove_line_item($order, $line_item_id) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  if ($order_wrapper) {
    $line_item_found = FALSE;

    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      if ($line_item_wrapper->line_item_id->raw() == $line_item_id) {
        $order_wrapper->commerce_line_items->offsetUnset($delta);
        $line_item_found = TRUE;
        break;
      }
    }

    if ($line_item_found) {
      if (commerce_line_item_delete($line_item_id)) {
        commerce_order_save($order);
        commerce_sos_commerce_line_item_updated($order);
      }
    }
    else {
      throw new Exception(t('Cannot remove line item, the order does not have a line item with ID @id', [
        '@id' => $line_item_id,
      ]));
    }
  }
  else {
    throw new Exception(t('Cannot remove line item, transaction does not have an order created for it.'));
  }
}

/**
 * Updates the quantity of a line item in the transactions' order.
 *
 * @param object $order
 *   The commerce order entity.
 * @param int $line_item_id
 *   The line item id.
 * @param int $quantity
 *   The number to update the line item by.
 * @param string $method
 *   Whether to update or replace the entire quantity.
 */
function commerce_sos_update_line_item_quantity($order, $line_item_id, $quantity, $method = 'replace') {
  $line_item = commerce_line_item_load($line_item_id);
  $existing_qty = $line_item->quantity;

  if ($method == 'update') {
    $new_qty = $existing_qty + $quantity;
  }
  else {
    $new_qty = $quantity;
  }

  // Make sure the line item actually belongs to the order.
  if ($new_qty > 0 && ($line_item->order_id == $order->order_id) && ((int) $existing_qty != $new_qty)) {
    $line_item->quantity = $new_qty;
    commerce_line_item_save($line_item);
    commerce_sos_commerce_line_item_updated($order);
  }
  elseif ($new_qty == 0) {
    commerce_sos_remove_line_item($order, $line_item_id);
    commerce_sos_after_delete_line_item($order);
  }
}

/**
 * Returns the header content that is attached to the /cart & checkout forms.
 *
 * @param int $order_id
 *   The order id.
 *
 * @return string
 *   Returns the html content.
 */
function commerce_sos_fetch_order_header_content($order_id) {
  $content = '<div class="commerce-sos order-id">
    <span class="label">Order ID:</span>
    <span class="field id-number">' . $order_id
    . '</span></div>';

  // Allow users to alter the content.
  drupal_alter('commerce_sos_order_header_content', $order_id, $content);

  return $content;
}

/**
 * Re-calculates and updates a line item entity.
 *
 * @param object $line_item
 *   The line item entity.
 * @param object $product
 *   The product entity.
 */
function commerce_sos_update_order($order, $line_item, $product) {
  entity_get_controller('commerce_line_item')->resetCache([$line_item->line_item_id]);
  commerce_cart_order_refresh($order);
}

/**
 * Removes the commerce pricing attribute data associate with a line item.
 *
 * @param object $line_item
 *   The line item entity.
 *
 * @return object
 *   The updated line item entity.
 */
function commerce_sos_remove_commerce_option_attributes($line_item) {
  $line_item->commerce_pricing_attributes = serialize([]);
  commerce_option_entity_delete($line_item, 'commerce_line_item');
  commerce_line_item_save($line_item);

  return $line_item;
}

/**
 * Add a shipping service to an order.
 *
 * @param object $order
 *   The order entity.
 * @param string $shipping_service
 *   The name of the shipping service.
 * @param object $rate_line_item
 *   The rate entity for the shipping service.
 */
function commerce_sos_add_shipping_service($order, $shipping_service, $rate_line_item) {
  // Delete any existing shipping line items from the order.
  commerce_shipping_delete_shipping_line_items($order, TRUE);

  // Allow users to alter the deletion of the shipping line items.
  drupal_alter('commerce_sos_delete_shipping_line_items', $shipping_service, $rate_line_item, $order);

  $rate_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $rate_line_item);
  $unit_price = $rate_line_item_wrapper->commerce_unit_price->value();

  // Create a new shipping line item with the calculated rate from the form.
  $line_item = commerce_shipping_line_item_new($shipping_service, $unit_price, $order->order_id, $rate_line_item->data, $rate_line_item->type);

  // Add the service details to the line item's data array and the order.
  $line_item->data['service_details'] = [];

  // Save and add the line item to the order.
  commerce_shipping_add_shipping_line_item($line_item, $order);
}

/**
 * Removes the shipping items from an order if empty line items.
 *
 * @param EntityMetadataWrapper $order_wrapper
 *   The order wrapper.
 */
function commerce_sos_remove_shipping_items(EntityMetadataWrapper $order_wrapper) {
  commerce_shipping_delete_shipping_line_items($order_wrapper->value(), TRUE);
}

/**
 * Applies a gift card to an order if it passes all GC criteria.
 *
 * @param object $order
 *   The order entity.
 * @param string $coupon_id
 *   The ID of the coupon.
 * @param int $amount
 *   The amount to apply to the gift card and subtract from the order total.
 *
 * @return bool|object
 *   Returns the saved order object if the gift card was successfully applied.
 */
function commerce_sos_apply_gift_card($order, $coupon_id, $amount) {
  // Make sure amount is either empty or a number greater than zero.
  if (empty($amount) || ($amount && (!is_numeric($amount) || $amount < 0))) {
    drupal_set_message(t('You have entered an invalid amount.'), 'error');
    return;
  }

  // Make sure the entered amount is not more than the remaining balance on
  // the order.
  $order_balance = commerce_payment_order_balance($order);
  $order_balance = commerce_currency_amount_to_decimal($order_balance['amount'], $order_balance['currency_code']);

  if ($amount > $order_balance) {
    drupal_set_message(t('The entered amount is greater than the remaining balance on the order total.'), 'error');
    return;
  }

  $amount = commerce_currency_decimal_to_amount($amount, commerce_default_currency());

  // Make sure the giftcard exists.
  $coupon = commerce_coupon_load($coupon_id);

  if (!$coupon || $coupon->type != 'giftcard_coupon') {
    drupal_set_message(t('This giftcard does not exist.'), 'error');
    return;
  }

  // Make sure there is not already a giftcard use line item referencing this
  // code.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $order_line_item_wrapper) {
    if ($order_line_item_wrapper->type->value() == 'giftcard_use' && $order_line_item_wrapper->commerce_giftcard->coupon_id->value() == $coupon->coupon_id) {
      drupal_set_message(t('You may not add the same giftcard code more than once per order.'), 'error');
      return;
    }
  }

  // Make sure it has a positive balance.
  $balance = commerce_gc_giftcard_balance($coupon->coupon_id);

  if ($balance <= 0) {
    drupal_set_message(t('This balance on this giftcard is not greater than zero.'), 'error');
    return;
  }

  // Make sure that the amount entered does not exceed the balance
  if ($amount && $amount > $balance) {
    $balance_display = commerce_currency_format($balance, commerce_default_currency());
    drupal_set_message(t('You have entered an amount greater than the balance on this card. Card balance is @balance.', array('@balance' => $balance_display)), 'error');
    return;
  }

  commerce_order_calculate_total($order);
  $order_total = $order_wrapper->commerce_order_total->amount->value();

  $ceiling_amount = $order_total < $balance ? $order_total : $balance;
  $line_item_amount = $amount ? $amount : $ceiling_amount;

  // This can happen if the order total is zero.
  if (!$line_item_amount) {
    drupal_set_message(t('You cannot add a zero amount giftcard use line item.'), 'error');
    return;
  }

  $line_item = entity_create('commerce_line_item', array(
    'type' => 'giftcard_use',
    'order_id' => $order_wrapper->order_id->value(),
    'quantity' => 1,
  ));
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Set the giftcard reference
  $line_item_wrapper->commerce_giftcard = $coupon;

  // Set the price
  $price = array(
    'amount' => -$line_item_amount,
    'currency_code' => commerce_default_currency(),
    'data' => array()
  );
  $line_item_wrapper->commerce_unit_price->amount = -$line_item_amount;
  $line_item_wrapper->commerce_unit_price->currency_code = commerce_default_currency();
  $base_price = array(
    'amount' => 0,
    'currency_code' => commerce_default_currency(),
    'data' => array(),
  );

  // Add the gift card to our order.
  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);
  $new_line_item = commerce_gc_add_line_item($order_wrapper, $coupon_wrapper, $price);
  // Reduce the amount we just used from the gift card.
  commerce_gc_transaction_write($coupon_id, -$line_item_amount);

  drupal_set_message(t('Successfully applied gift card to order.'));

  return $order;
}

/**
 * Fetch all gift cards applied to an order.
 *
 * @param object $order
 *   The order entity.
 *
 * @return array
 *   An array of gift card coupons keyed on the coupon id.
 */
function commerce_sos_get_fetch_gift_cards_for_order($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $existing_coupon_ids = [];
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'giftcard_use') {
      $coupon_id = $line_item_wrapper->commerce_giftcard->coupon_id->value();
      $coupon = commerce_coupon_load($coupon_id);
      $existing_coupon_ids[$coupon_id] = [
        'code' => $coupon->code,
        'amount_applied' => $line_item_wrapper->commerce_unit_price->amount->value(),
      ];
    }
  }

  return $existing_coupon_ids;
}

/**
 * Removes a gift card from an order.
 *
 * @param object $order
 *   The order entity.
 * @param $coupon_id
 *   The ID of the coupon to remove.
 *
 * @return bool|object
 *   Returns a saved $order object, FALSE if it wasn't removed.
 */
function commerce_sos_remove_gift_card_from_order($order, $coupon_id) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $gift_card_found = FALSE;
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'giftcard_use') {
      if ($coupon_id == $line_item_wrapper->commerce_giftcard->coupon_id->value()) {
        $gift_card_found = TRUE;

        // Remove the gift card coupon and line item from the order.
        $coupon = commerce_coupon_load($coupon_id);
        // Remove the coupon from the order.
        commerce_coupon_remove_coupon_from_order($order, $coupon, FALSE);

        // Add back the amount we used from the gift card for this order.
        // We'll add a negative to the amount as it is stored as a negative in
        // the line item, that way the two negatives will make a positive.
        commerce_gc_transaction_write($coupon_id, -$line_item_wrapper->commerce_unit_price->amount->value());

        // Remove the line item from the order.
        $order_wrapper->commerce_line_items->offsetUnset($delta);
        commerce_line_item_delete($line_item_wrapper->line_item_id->value());

        commerce_order_save($order);

        drupal_set_message(t('Gift card removed from order.'));

        return $order;
      }
    }
  }

  $message = t('Unable to remove gift card from order.');
  if (!$gift_card_found) {
    $message = t('The gift card could not be found in the order.');
  }

  drupal_set_message($message);

  return FALSE;
}

/**
 * Determine whether an order has items which are out of stock.
 *
 * @param object $order
 *   The order entity.
 *
 * @return array
 *   Returns an array of products that are out of stock.
 */
function commerce_sos_order_has_out_of_stock($order) {
  $products = [];

  if (module_exists('commerce_ss')) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    // Check each line item.
    foreach ($order_wrapper->commerce_line_items as $index => $line_item_wrapper) {
      if (in_array($line_item_wrapper->getBundle(), commerce_product_line_item_types())) {
        $product_id = $line_item_wrapper->commerce_product->product_id->value();
        $product = commerce_product_load($product_id);
        $qty_ordered = $line_item_wrapper->quantity->value();

        // Check using rules.
        commerce_stock_check_product_checkout_rule($product, $qty_ordered, $stock_state, $message);

        // Both 1 and 2 are errors.
        if (($stock_state == 1)|| ($stock_state == 2)) {
          $products[$product_id] = $product->title . ' (' . $product->sku . ')';
        }
      }
    }
  }

  return $products;
}

/**
 * Print the product details for a line item in an order.
 *
 * @param array $line_item_details
 *   The array of line_item details.
 * @param int $quantity_ordered
 *   The current quantity ordered of this item.
 *
 * @return string
 *   The html for the product details.
 */
function _commerce_print_added_product_details(array $line_item_details, $quantity_ordered) {
  $output = '<div id="product-image">' . commerce_sos_product_thumbnail($line_item_details['product_info']['product']) . '</div>';

  $output .= '<div id="product-details">';
  $output .= '<div id="product-title">' . $line_item_details['product_info']['title'] . '</div>';
  $output .= '<div id="product-attributes">' . implode('<br>', $line_item_details['product_info']['product_attributes']) . '</div>';
  $output .= '<div id="product-quantity">' . t('Quantity Purchased: ') . $quantity_ordered . '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Adjust the stock depending on the quantity.
 *
 * @param array $product_info
 *   The product details array.
 * @param int $quantity
 *   The quantity we're adjusting.
 */
function commerce_sos_adjust_stock(array $product_info, $quantity) {
  $product = $product_info['product'];

  if (module_exists('commerce_ss')) {
    module_load_include('inc', 'commerce_ss', 'commerce_ss.rules');

    if (commerce_ss_product_type_enabled($product->type)) {
      if (!(commerce_ss_product_type_override_enabled($product->type)
        && isset($product->commerce_stock_override[LANGUAGE_NONE]) && $product->commerce_stock_override[LANGUAGE_NONE][0]['value'] == 1)
      ) {
        // Add/remove the quantity to/from the available stock level.
        commerce_ss_stock_adjust($product, $quantity);
      }
    }
  }
}

/**
 * Cancel an order.
 *
 * @param object $order
 *   The order entity.
 */
function commerce_sos_void_order($order) {
  $status = 'canceled';
  $save_type = 'void_order';

  // Allow users to alter the status.
  drupal_alter('commerce_sos_commerce_order_save', $order, $save_type, $status);

  // Finally, save the status.
  commerce_order_status_update($order, $status);
}

/**
 * Performs actions once an order line item has been updated.
 *
 * @param object $order
 *   The order entity.
 */
function commerce_sos_commerce_line_item_updated($order) {
  // Update existing order discounts.
  commerce_sos_discount_update_order_discounts($order);
}
