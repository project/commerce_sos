<?php

/**
 * @file
 * Contains code to add the vertical tab field elements to the sos order form.
 */

/**
 * Add the vertical tab elements.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 *
 * @return array
 *   The form array.
 */
function _commerce_sos_add_vertical_tab_fields(array $form, array &$form_state) {
  $order = $form_state['commerce_order'];

  $form['additional_settings'] = [
    '#type' => 'vertical_tabs',
    '#prefix' => '<div id="sos-vertical-tabs-wrapper">',
    '#suffix' => '</div>',
  ];

  // Shipping Tab.
  _commerce_sos_add_shipping_tab($form['vertical_tabs'], $form_state);

  // Order Discount Tab.
  if (module_exists('commerce_discount')) {
    _commerce_sos_add_order_discount_tab($form['vertical_tabs'], $form_state);
  }

  // Coupons Tab.
  if (module_exists('commerce_discount') && module_exists('commerce_coupon')) {
    _commerce_sos_add_coupons_tab($form['vertical_tabs'], $form_state);
  }

  // Notes Tab.
  _commerce_sos_add_notes_tab($form['vertical_tabs'], $form_state);

  // Order Status Tab.
  _commerce_sos_add_order_status_tab($form['vertical_tabs'], $form_state);

  // Advanced Tab.
  _commerce_sos_add_advanced_tab($form['vertical_tabs'], $form_state);

  // Add the default shipping service charge to the order.
  if (module_exists('commerce_shipping')) {
    if (isset($form['vertical_tabs']['shipping']['shipping_methods']['shipping_service'])) {
      $shipping_service_selected = $form['vertical_tabs']['shipping']['shipping_methods']['shipping_service']['#default_value'];

      // Now, save the new shipping service to the order.
      if ($form_state['total_product_quantity'] > 0) {
        commerce_sos_add_shipping_service($order, $shipping_service_selected, $form['vertical_tabs']['shipping']['shipping_methods']['shipping_rates']['#value'][$shipping_service_selected]);
      }
    }
  }

  // Detect ajax clicks.
  if (isset($form_state['triggering_element'])) {
    if ($form_state['triggering_element']['#element_key']) {
      $form_state['order_updated'] = TRUE;
    }
    else {
      // If the shipping service was updated, save it to the order.
      if ($form_state['triggering_element']['#name'] == 'vertical_tabs[shipping][shipping_methods][shipping_service]') {
        // Exit if it's not shipable.
        if (!commerce_physical_order_shippable($order)) {
          return FALSE;
        }

        // Get the shipping service selected.
        // If in the form state input.
        if (isset($form_state['input']['vertical_tabs']['shipping']['shipping_methods']['shipping_service'])) {
          $shipping_service_selected = $form_state['input']['vertical_tabs']['shipping']['shipping_methods']['shipping_service'];
        }
        // Else if in the form state values.
        elseif (isset($form_state['values']['vertical_tabs']['shipping']['shipping_methods']['shipping_service'])) {
          $shipping_service_selected = $form_state['values']['vertical_tabs']['shipping']['shipping_methods']['shipping_service'];
        }
        // Else, get the default value.
        else {
          $shipping_service_selected = $form['vertical_tabs']['shipping']['shipping_methods']['shipping_service']['#default_value'];
        }

        // Now, save the new shipping service to the order.
        commerce_sos_add_shipping_service($order, $shipping_service_selected, $form['vertical_tabs']['shipping']['shipping_methods']['shipping_rates']['#value'][$shipping_service_selected]);

        $form_state['order_updated'] = TRUE;
      }
    }
  }

  return $form;
}

/**
 * Ajax callback to replace the order summary total, prods, and vertical tabs.
 */
function _commerce_sos_vertical_tabs_ajax_callback($form, &$form_state) {
  $commands = [];
  $commands[] = ajax_command_replace('#sos-products-table-wrapper', drupal_render($form['products']));
  $commands[] = ajax_command_remove('div.messages');
  $commands[] = ajax_command_replace('#sos-vertical-tabs-wrapper', drupal_render($form['additional_settings']));
  $commands[] = ajax_command_prepend('#sos-vertical-tabs-wrapper', theme('status_messages'));
  $commands[] = ajax_command_replace('#sos-order-total-summary', drupal_render($form['order_total_summary']));
  $commands[] = ajax_command_replace('#sos-finish-buttons-wrapper', drupal_render($form['finish_buttons']));
  $commands[] = [
    'command' => 'setActiveTab',
    'selector' => 'sos-vertical-tabs-wrapper',
    'sub_selector' => 'ul.vertical-tabs-list',
    'index'  => _commerce_sos_get_vertical_tabs_index($form_state['triggering_element']),
  ];

  return ['#type' => 'ajax', '#commands' => $commands];
}

/**
 * Display the shipping tab.
 */
function _commerce_sos_add_shipping_tab(&$element, &$form_state) {
  // Display it only if we have shipping enabled and have shippable products.
  if (module_exists('commerce_shipping') && module_exists('commerce_physical') && commerce_physical_order_shippable($form_state['commerce_order'])) {
    form_load_include($form_state, 'inc', 'commerce_shipping', 'includes/commerce_shipping.checkout_pane');
    $checkout_pane = commerce_checkout_pane_load('commerce_shipping');

    $element['shipping'] = [
      '#type' => 'fieldset',
      '#title' => t('Shipping'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'additional_settings',
    ];

    // Add the shipping method fields.
    $element['shipping']['shipping_methods'] = [
      '#type' => 'fieldset',
      '#title' => t('Shipping Methods'),
    ];
    $element['shipping']['shipping_methods'] += commerce_shipping_pane_checkout_form($element, $form_state, $checkout_pane, $form_state['commerce_order']);

    // Let's update the shipping service ajax method to use ours.
    $element['shipping']['shipping_methods']['shipping_service']['#element_key'] = 'shipping-service-update';
    $element['shipping']['shipping_methods']['shipping_service']['#ajax'] = [
      'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
      'wrapper' => 'sos-order-total-summary',
    ];

    if (!$form_state['edit_order']) {
      $element['shipping']['shipping_methods']['shipping_service']['#attributes']['disabled'] = TRUE;
    }
  }
}

/**
 * Display the notes tab.
 */
function _commerce_sos_add_notes_tab(&$element, &$form_state) {
  // Add a section to display current notes and add new notes.
  $element['notes'] = [
    '#type' => 'fieldset',
    '#title' => t('Notes'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
  ];

  $element['notes']['previous_notes'] = [
    '#type' => 'item',
    '#markup' => '<h3 class="sos-view-title">' . t('Notes') . '</h3>' . commerce_embed_view('commerce_sos_order_messages', 'default', [$form_state['commerce_order']->order_id]),
  ];

  $element['notes']['new_note'] = [
    '#type' => 'textarea',
    '#title' => '',
    '#attributes' => [
      'placeholder' => t('Add any important notes about this order...'),
    ],
    '#prefix' => '<div id="sos-new-note-wrapper">',
    '#suffix' => '</div>',
    '#rows' => 4,
  ];

  $element['notes']['add_note'] = [
    '#type' => 'submit',
    '#value' => t('Add Note'),
    '#suffix' => '<div id="sos-add-note-important-description">' . t('Hold up! <span>Notes will be displayed to the customer</span>, so choose your words wisely.') . '</div>' . '<div id="sos-add-note-description">' . t('Use the Order Status Log to make notes for internal use.') . '</div>',
    '#validate' => ['_commerce_sos_validate_note'],
    '#submit' => ['_commerce_sos_submit_note'],
    '#ajax' => [
      'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
    ],
  ];
}

/**
 * Display the order status tab.
 */
function _commerce_sos_add_order_status_tab(&$element, &$form_state) {
  // Build an array of order status options grouped by order state.
  $options = [];

  foreach (commerce_order_state_get_title() as $name => $title) {
    foreach (commerce_order_statuses(['state' => $name]) as $order_status) {
      $options[check_plain($title)][$order_status['name']] = check_plain($order_status['title']);
    }
  }

  // Add a section to update the status and leave a log message.
  $element['order_status'] = [
    '#type' => 'fieldset',
    '#title' => t('Order status'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
  ];

  $element['order_status']['status'] = [
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => $options,
    '#default_value' => $form_state['commerce_order']->status,
  ];

  $element['order_status']['status_original'] = [
    '#type' => 'hidden',
    '#value' => $form_state['commerce_order']->status,
    '#attributes' => ['id' => ['edit-vertical-tabs-order-status-original']],
  ];

  $element['order_status']['revision'] = [
    '#type' => 'item',
    '#title' => t('Order Revisions'),
    '#markup' => commerce_embed_view('commerce_sos_order_revisions', 'revision_block', [$form_state['commerce_order']->order_id]),
  ];

  $element['order_status']['history'] = [
    '#type' => 'item',
    '#title' => t('Order History'),
    '#markup' => commerce_embed_view('commerce_sos_order_history', 'history_block', [$form_state['commerce_order']->order_id]),
  ];

  $element['order_status']['log'] = [
    '#type' => 'textarea',
    '#title' => t('Update log message/Add admin comment'),
    '#attributes' => [
      'placeholder' => t('Provide an explanation of the changes you are making. This will provide a meaningful audit trail for updates to this order.'),
    ],
    '#rows' => 4,
  ];

  $element['order_status']['update_status'] = [
    '#type' => 'submit',
    '#value' => t('Update Status'),
    '#suffix' => '<div id="sos-update-status-description">' . t('Status notes are for internal use only, they won\'t be seen by the customer.') . '</div>',
    '#submit' => ['_commerce_sos_update_status_submit'],
  ];
}

/**
 * Validate function for the add note button.
 */
function _commerce_sos_validate_note($form, &$form_state) {
  if (empty($form_state['values']['vertical_tabs']['notes']['new_note'])) {
    form_set_error('notes][new_note', t('Note cannot be empty'));
  }
}

/**
 * Submit function for the add note button.
 */
function _commerce_sos_submit_note($form, &$form_state) {
  global $user;

  // Create a new commerce order user comment message and save it.
  $message = message_create('commerce_order_user_comment', ['uid' => $user->uid]);
  try {
    $wrapper = entity_metadata_wrapper('message', $message);
    $wrapper->message_commerce_order->set($form_state['commerce_order']->order_id);
    $wrapper->message_commerce_body->set(['value' => $form_state['values']['vertical_tabs']['notes']['new_note']]);
    $wrapper->save();

    $form_state['input'] = [];
    $form_state['rebuild'] = TRUE;

    drupal_set_message(t('A new order comment has been successfully added.'));
  }
  catch (EntityMetadataWrapperException $exc) {
    drupal_set_message(t('There was an error saving the log message.'), 'error');
  }
}

/**
 * Submit function for the update status button.
 */
function _commerce_sos_update_status_submit($form, &$form_state) {
  global $user;

  // Update the order status along with the log message.
  $log_message = $form_state['values']['vertical_tabs']['order_status']['log'];

  // Create a new commerce order admin comment message and save it.
  // We're using this intsead of adding it to the status update function because
  // that function won't save if the status hasn't changed.
  if (!empty($log_message) && $form_state['values']['vertical_tabs']['order_status']['status'] == $form_state['commerce_order']->status) {
    $message = message_create('commerce_order_admin_comment', ['uid' => $user->uid]);
    try {
      $wrapper = entity_metadata_wrapper('message', $message);
      $wrapper->message_commerce_order->set($form_state['commerce_order']->order_id);
      $wrapper->message_commerce_body->set(['value' => $log_message]);
      $wrapper->save();
    }
    catch (EntityMetadataWrapperException $exc) {
      drupal_set_message(t('There was an error saving the log message.'), 'error');
    }
  }
  // Else, if the status has changed, just update the order status with the
  // new log message.
  else {
    commerce_order_status_update($form_state['commerce_order'], $form_state['values']['vertical_tabs']['order_status']['status'], FALSE, NULL, $log_message);
  }

  drupal_set_message(t('Successfully updated order status to @status.', ['@status' => $form_state['values']['vertical_tabs']['order_status']['status']]));
}

/**
 * Display the advanced tab.
 */
function _commerce_sos_add_advanced_tab(&$element, &$form_state) {
  // Add a section to update the status and leave a log message.
  $element['advanced'] = [
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
  ];

  $element['advanced']['advanced_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Advanced Settings'),
    '#description' => t('Unless you know what you are doing you probably shouldn\'t change any of this stuff.'),
    '#attributes' => ['class' => ['advanced-vertical-tabs']],
  ];

  // Display all the fields that the user selected in the admin settings.
  if (!empty(variable_get('commerce_sos_order_form_advanced_fields'))) {
    foreach (variable_get('commerce_sos_order_form_advanced_fields') as $field_name) {
      $lang_code = field_language('commerce_order', $form_state['commerce_order'], $field_name);
      field_attach_form('commerce_order', $form_state['commerce_order'], $element['advanced']['advanced_settings'], $form_state, $lang_code, array('field_name' => $field_name));

      // Disable the field elements if we're not in editing mode.
      if (!$form_state['edit_order']) {
        foreach (element_children($element['advanced']['advanced_settings']) as $key => $field) {
          $element['advanced']['advanced_settings'][$field]['#after_build']['disable'] = '_commerce_sos_disable_after_build';
        }
      }
    }
  }

  // Finally, our save button.
  $element['advanced']['save_changes'] = [
    '#type' => 'submit',
    '#value' => t('Save Changes'),
    '#submit' => ['_commerce_sos_advanced_settings_submit'],
    '#validate' => ['_commerce_sos_advanced_settings_validate'],
    '#ajax' => [
      'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
    ],
    '#element_key' => 'save-advanced-settings',
    '#name' => 'save-advanced-settings',
  ];

  // Disable the save button if we're not in edit mode.
  if (!$form_state['edit_order']) {
    $element['advanced']['save_changes']['#attributes']['disabled'] = TRUE;
  }
}

/**
 * Validate function for the advanced settings save changes button.
 */
function _commerce_sos_advanced_settings_validate($form, &$form_state) {
  field_attach_form_validate('commerce_order', $form_state['commerce_order'], $form['vertical_tabs']['advanced']['advanced_settings'], $form_state);
}

/**
 * Submit function for the advanced settings save changes button.
 */
function _commerce_sos_advanced_settings_submit($form, &$form_state) {
  foreach ($form_state['values']['vertical_tabs']['advanced']['advanced_settings'] as $field_name => $value) {
    $order = $form_state['commerce_order'];
    $order->{$field_name} = $value;
    field_attach_update('commerce_order', $order);
    entity_get_controller('commerce_order')->resetCache(array($order->order_id));
    $form_state['commerce_order'] = $order;
  }

  $form_state['order_updated'] = TRUE;
  $form_state['rebuild'] = TRUE;

  drupal_set_message(t('Successfully saved changes to order.'));
}

/**
 * Display the add order discount tab.
 */
function _commerce_sos_add_order_discount_tab(&$element, &$form_state) {
  // Add a section to add order discounts.
  $element['order_discount'] = [
    '#type' => 'fieldset',
    '#title' => t('Order Discount'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
  ];

  $element['order_discount']['add_order_discount'] = [
    '#type' => 'fieldset',
    '#title' => t('Order Discount'),
    '#prefix' => '<div id="sos-add-order-discount-wrapper">',
    '#suffix' => '</div>',
  ];

  // Show added order discount, if a discount was added.
  $order_discount = _commerce_sos_order_discount_exists($form_state['commerce_order']);
  if ($order_discount) {
    if ($form_state['edit_order']) {
      $element['order_discount']['add_order_discount']['remove_order_discount'] = [
        '#type' => 'button',
        '#value' => t('Remove'),
        '#prefix' => '<div id="sos-previous-order-discount"><div id="sos-order-discount-value">' . t('Current Order Discount: @order_discount -', ['@order_discount' => $order_discount]) . '</div>',
        '#element_key' => 'remove-order-discount',
        '#ajax' => [
          'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
        ],
        '#suffix' => '</div>',
      ];
    }
    else {
      $element['order_discount']['add_order_discount']['remove_order_discount'] = [
        '#type' => 'item',
        '#prefix' => '<div id="sos-previous-order-discount"><div id="sos-order-discount-value">' . t('Current Order Discount: @order_discount', ['@order_discount' => $order_discount]) . '</div>',
        '#suffix' => '</div>',
      ];
    }
  }

  $element['order_discount']['add_order_discount']['amount'] = [
    '#type' => 'textfield',
    '#description' => t('This Discount is applied to the total order.'),
    '#size' => 6,
    '#maxlength' => 6,
  ];

  $element['order_discount']['add_order_discount']['type'] = [
    '#type' => 'select',
    '#options' => [
      'percent' => t('% - Percent'),
      'fixed' => t('$ - Dollar Amount'),
    ],
  ];

  $element['order_discount']['add_order_discount']['apply'] = [
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#validate' => ['_commerce_sos_add_order_discount_validate'],
    '#submit' => ['_commerce_sos_add_order_discount_submit'],
    '#ajax' => [
      'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
    ],
    '#element_key' => 'add-order-discount',
    '#name' => 'add-order-discount',
  ];

  // If we're not in edit mode, disable the fields.
  if (!$form_state['edit_order']) {
    $element['order_discount']['add_order_discount']['amount']['#attributes']['disabled'] = TRUE;
    $element['order_discount']['add_order_discount']['type']['#attributes']['disabled'] = TRUE;
    $element['order_discount']['add_order_discount']['apply']['#attributes']['disabled'] = TRUE;
  }
}

/**
 * Validate function for the add order discount apply button.
 */
function _commerce_sos_add_order_discount_validate($form, &$form_state) {
  if (empty($form_state['values']['vertical_tabs']['order_discount']['add_order_discount']['amount'])) {
    form_set_error('', t('Order discount cannot be empty.'));
  }
  elseif (!is_numeric($form_state['values']['vertical_tabs']['order_discount']['add_order_discount']['amount']) && $form_state['values']['vertical_tabs']['order_discount']['add_order_discount']['amount'] > 0) {
    form_set_error('', t('Discount needs to be a positive number.'));
  }
}

/**
 * Submit function for the add order discount apply button.
 */
function _commerce_sos_add_order_discount_submit($form, &$form_state) {
  // Reset the form and add a success message.
  $form_state['input'] = [];
  $form_state['rebuild'] = TRUE;

  drupal_set_message(t('Order discount has been successfully added.'));
}

/**
 * Returns the order discount amount if an order discount has been set.
 *
 * @param object $order
 *   The order entity.
 *
 * @return bool|int
 *   Returns the discount amount if a discount is set, FALSE otherwise.
 */
function _commerce_sos_order_discount_exists($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $price = $order_wrapper->commerce_order_total->value();
  foreach ($price['data']['components'] as $key => $component) {
    if (isset($component['price']['data']['discount_name']) && $component['price']['data']['discount_name'] == COMMERCE_SOS_ORDER_DISCOUNT_NAME) {
      $formatted_price = commerce_currency_format(
        abs($component['price']['amount']),
        $component['price']['currency_code'],
        $order_wrapper->value()
      );

      return $formatted_price;
    }
  }

  return FALSE;
}

/**
 * Display the coupons tab.
 */
function _commerce_sos_add_coupons_tab(&$element, &$form_state) {
  // Add a section to add coupon discounts.
  $element['coupons'] = [
    '#type' => 'fieldset',
    '#title' => t('Coupons'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
  ];

  $element['coupons']['add_coupon'] = [
    '#type' => 'fieldset',
    '#title' => t('Coupons'),
    '#prefix' => '<div id="sos-add-coupon-wrapper">',
    '#suffix' => '</div>',
  ];

  _commerce_sos_add_existing_coupon_discounts($element, $form_state);

  $element['coupons']['add_coupon']['search'] = [
    '#type' => 'textfield',
    '#description' => t('Search for Coupon or Discount Code.<div id="sos-gift-cart-text">Gift Cards can be applied as Payment Methods on the next page.</div>'),
    '#autocomplete_path' => 'commerce/sos/coupons/find',
  ];

  $element['coupons']['add_coupon']['apply_coupon'] = [
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#validate' => ['_commerce_sos_add_coupon_validate'],
    '#submit' => ['_commerce_sos_add_coupon_submit'],
    '#ajax' => [
      'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
    ],
    '#element_key' => 'add-coupon',
    '#name' => 'add-coupon',
  ];

  // If we're not in edit mode, disable the fields.
  if (!$form_state['edit_order']) {
    $element['coupons']['add_coupon']['search']['#attributes']['disabled'] = TRUE;
    $element['coupons']['add_coupon']['apply_coupon']['#attributes']['disabled'] = TRUE;
  }
}

/**
 * Validate function for the add coupon apply button.
 */
function _commerce_sos_add_coupon_validate($form, &$form_state) {
  if (empty($form_state['values']['vertical_tabs']['coupons']['add_coupon']['search'])) {
    form_set_error('coupons][add_coupon][search', t('Coupon field cannot be empty.'));
  }
}

/**
 * Submit function for the add coupon apply button.
 */
function _commerce_sos_add_coupon_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Displays the existing coupons in an order as form elements.
 */
function _commerce_sos_add_existing_coupon_discounts(&$element, &$form_state) {
  $order = $form_state['commerce_order'];
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  if (!empty($order->commerce_coupons)) {
    // Go through each coupon in the order and display info about them.
    foreach ($order_wrapper->commerce_coupons as $delta => $coupon_wrapper) {
      // Load the coupon.
      $coupon = commerce_coupon_load($coupon_wrapper->coupon_id->value());

      $element['coupons']['add_coupon']['existing_coupons'][$coupon->code] = [
        '#type' => 'fieldset',
        '#prefix' => '<div class="sos-coupon-discount-' . $coupon->code . '">',
        '#suffix' => '</div>',
      ];

      // The info about the coupon.
      $element['coupons']['add_coupon']['existing_coupons'][$coupon->code]['info'] = [
        '#type' => 'item',
        '#title' => $coupon->code,
        '#markup' => commerce_sos_display_coupon_discount_value($order, $coupon),
      ];

      // The remove button to remove the coupon from the order.
      $element['coupons']['add_coupon']['existing_coupons'][$coupon->code]['remove'] = [
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#ajax' => [
          'callback' => '_commerce_sos_vertical_tabs_ajax_callback',
        ],
        '#submit' => ['_commerce_sos_remove_coupon_submit'],
        '#coupon_id' => $coupon->coupon_id,
        '#name' => 'remove-coupon-' . $coupon->code,
        '#element_key' => 'remove-coupon',
        '#prefix' => '<div class="sos-previous-coupon-remove"><span>-</span>',
        '#suffix' => '</div>',
      ];

      // If we're not in edit mode, disable the fields.
      if (!$form_state['edit_order']) {
        $element['coupons']['add_coupon']['existing_coupons'][$coupon->code]['info']['#attributes']['disabled'] = TRUE;
        $element['coupons']['add_coupon']['existing_coupons'][$coupon->code]['remove']['#access'] = FALSE;
      }
    }
  }
}

/**
 * Submit function for the remove coupon button.
 */
function _commerce_sos_remove_coupon_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  drupal_set_message(t('Coupon removed from order.'));
}

/**
 * Returns the index of the vertical tab depending on what was clicked.
 *
 * @param array $triggering_element
 *   The triggering element.
 *
 * @return int
 *   The index of the clicked element.
 */
function _commerce_sos_get_vertical_tabs_index(array $triggering_element) {
  $vertical_tabs_array = [
    'shipping' => 1,
    'order_discount' => 2,
    'coupons' => 3,
    'notes' => 4,
    'order_status' => 5,
    'advanced' => 6,
  ];

  return $vertical_tabs_array[$triggering_element['#parents'][1]];
}
