<?php

/**
 * @file
 * Contains code to add the payment section to the sos order form.
 */

/**
 * Add the payment elements.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 *
 * @return array
 *   The form array.
 */
function _commerce_sos_add_payment_fields(array $form, array &$form_state) {
  // Attaches css and js from the field group module.
  $form['#attached']['css'] = [
    drupal_get_path('module', 'field_group') . '/horizontal-tabs/horizontal-tabs.css',
  ];
  $form['#attached']['js'] = [
    drupal_get_path('module', 'field_group') . '/horizontal-tabs/horizontal-tabs.js',
  ];

  // Defines the main tabs element.
  $form['horizontal_tabs'] = [
    '#type' => 'horizontal_tabs',
    '#tree' => TRUE,
    '#prefix' => '<div id="sos-horizontal-tabs-wrapper">',
    '#suffix' => '</div>',
  ];

  // Get the order total amount and currency code.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $form_state['commerce_order']);
  $order_total_amount = $order_wrapper->commerce_order_total->amount->value();
  $currency_code = $order_wrapper->commerce_order_total->currency_code->value();

  // Display it only if we have commerce payment enabled.
  if (module_exists('commerce_payment')) {
    // Otherwise present the payment method selection form.
    $event = rules_get_cache('event_commerce_payment_methods');

    if (!empty($event)) {

      $form_state['payment_method'] = commerce_payment_method_instance_load(variable_get('commerce_sos_credit_card_payment_service', ''));

      if ($form_state['payment_method']) {
        $form['horizontal_tabs']['payment_terminal'] = [
          '#type' => 'fieldset',
          '#title' => t('Credit Card'),
          '#attributes' => ['class' => ['payment-terminal']],
          '#element_validate' => array('_commerce_sos_payment_terminal_validate'),
        ];

        // Establish defaults for the amount if possible.
        if ($balance = commerce_payment_order_balance($form_state['commerce_order'])) {
          $default_amount = $balance['amount'] > 0 ? $balance['amount'] : '';
          $default_currency_code = $balance['currency_code'];

          // Convert the default amount to an acceptable textfield value.
          $default_amount = commerce_currency_amount_to_decimal($default_amount, $default_currency_code);

          $currency = commerce_currency_load($default_currency_code);
          $default_amount = number_format($default_amount, $currency['decimals'], '.', '');

          // Display the balance amount that needs to be paid message.
          if ($balance['amount'] == $order_total_amount) {
            $message = '<div id="remaining-amount">' . t('@balance must be paid to complete this order.', ['@balance' => commerce_currency_format($balance['amount'], $balance['currency_code'])]) . '</div>';
          }
          else {
            $already_paid_amount = $order_total_amount - $balance['amount'];
            $message = '<div id="processed-amount">' . t('Payment of <span>@already_paid_amount</span> successfully processed.', ['@already_paid_amount' => commerce_currency_format($already_paid_amount, $default_currency_code)]) . '</div>';
            if ($balance['amount'] > 0) {
              $message .= '<div id="remaining-amount">' . t('<span>@balance</span> is remaining and must be paid to complete this order.', ['@balance' => commerce_currency_format($balance['amount'], $balance['currency_code'])]) . '</div>';
            }
            else {
              $message .= '<div id="payment-completed">' . t('Payment Completed for Order!') . '</div>';
            }
          }
          $form['horizontal_tabs']['payment_terminal']['balance_amount'] = [
            '#type' => 'item',
            '#markup' => '<div id="sos-balance-message">' . $message . '</div>',
          ];
        }
        else {
          $default_amount = '';
          $default_currency_code = commerce_default_currency();
        }

        // Need this here to set the default vault after ajax submit.
        if (isset($form_state['triggering_element'])) {
          unset($form_state['input']['horizontal_tabs']['payment_terminal']['amount']);
        }
        $form['horizontal_tabs']['payment_terminal']['amount'] = [
          '#type' => 'textfield',
          '#title' => t('Amount'),
          '#default_value' => $default_amount,
          '#required' => TRUE,
          '#size' => 10,
          '#prefix' => '<div class="payment-terminal-amount">',
        ];

        // Build a currency options list from all enabled currencies.
        $options = [];

        foreach (commerce_currencies(TRUE) as $curr_code => $currency) {
          $options[$curr_code] = check_plain($currency['code']);
        }

        $form['horizontal_tabs']['payment_terminal']['currency_code'] = [
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => $default_currency_code,
          '#suffix' => '</div>',
        ];

        $form['horizontal_tabs']['payment_terminal']['amount_type'] = [
          '#type' => 'select',
          '#options' => ['full_amount' => t('Full Amount'), 'partial_amount' => t('Partial Amount')],
        ];

        // Find the values already submitted for the payment terminal.
        $terminal_values = !empty($form_state['values']['horizontal_tabs']['payment_details']) ? $form_state['values']['horizontal_tabs']['payment_details'] : [];

        if ($callback = commerce_payment_method_callback($form_state['payment_method'], 'submit_form')) {
          $form['horizontal_tabs']['payment_terminal']['payment_details'] = $callback($form_state['payment_method'], $terminal_values, NULL, $form_state['commerce_order']);
        }
        else {
          $form['horizontal_tabs']['payment_terminal']['payment_details'] = [];
        }

        $form['horizontal_tabs']['payment_terminal']['payment_details']['#tree'] = TRUE;

        $form['horizontal_tabs']['payment_terminal']['add_payment'] = [
          '#type' => 'submit',
          '#value' => t('Add Payment'),
          '#validate' => ['_commerce_sos_add_payment_validate'],
          '#submit' => ['_commerce_sos_horizontal_tabs_submit'],
          '#ajax' => [
            'callback' => '_commerce_sos_horizontal_tabs_ajax_callback',
          ],
          '#element_key' => 'add-payment',
          '#prefix' => '<div id="sos-spacer"></div>',
        ];

        if ($balance['amount'] == 0) {
          $form['horizontal_tabs']['payment_terminal']['print_order_details'] = [
            '#type' => 'link',
            '#title' => t('Print Order'),
            '#href' => 'admin/commerce/orders/' . $form_state['commerce_order']->order_id . '/print',
            '#attributes' => [
              'class' => ['button', 'print-order-details', 'blue-primary-button'],
              'target' => '_blank',
            ],
          ];
        }
      }
    }
  }

  // Display it only if we have commerce giftcard module is enabled.
  if (module_exists('commerce_gc')) {
    $form['horizontal_tabs']['gift_card'] = [
      '#type' => 'fieldset',
      '#title' => t('Gift Card'),
      '#attributes' => ['class' => ['gift-card']],
    ];

    $form['horizontal_tabs']['gift_card']['balance_amount'] = [
      '#type' => 'item',
      '#markup' => '<div id="sos-balance-message">' . $message . '</div>',
    ];

    // Search for a gift card.
    $form['horizontal_tabs']['gift_card']['search'] = [
      '#type' => 'textfield',
      '#title' => t('Gift Card'),
      '#description' => t('Enter Gift Card Code'),
    ];

    $form['horizontal_tabs']['gift_card']['find'] = [
      '#type' => 'submit',
      '#value' => t('Find'),
      '#validate' => ['_commerce_sos_gift_card_search_validate'],
      '#submit' => ['_commerce_sos_gift_card_search_submit'],
      '#ajax' => [
        'callback' => '_commerce_sos_horizontal_tabs_ajax_callback',
      ],
      '#element_key' => 'gift-card-find',
      '#name' => 'gift-card-find',
      '#limit_validation_errors' => [['horizontal_tabs', 'gift_card']],
    ];

    // Display previously applied gift cards.
    $form['horizontal_tabs']['gift_card']['previous_gift_cards_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Applied Gift Cards'),
      '#theme' => 'commerce_sos_gift_card_tables',
      '#attributes' => ['class' => ['element-invisible']],
      '#prefix' => '<div id="sos-previous-gc-fieldset">',
      '#suffix' => '</div>',
      '#empty_result_text' => '',
      '#header' => [],
      '#table_id' => 'sos-previous-gift-cards-table',
    ];

    // Grab the previous coupon details.
    $previous_coupons = commerce_sos_get_fetch_gift_cards_for_order($form_state['commerce_order']);
    if (!empty($previous_coupons)) {
      foreach ($previous_coupons as $key => $coupon_details) {
        $form['horizontal_tabs']['gift_card']['previous_gift_cards_fieldset']['#attributes']['class'] = [];

        // GC Code.
        $form['horizontal_tabs']['gift_card']['previous_gift_cards_fieldset'][$key]['gc_code'] = [
          '#type' => 'item',
          '#title' => t('GC Code: '),
          '#title_display' => 'before',
          '#markup' => $coupon_details['code'],
        ];

        // Applied amount.
        $form['horizontal_tabs']['gift_card']['previous_gift_cards_fieldset'][$key]['applied'] = [
          '#type' => 'item',
          '#title' => t('Applied: '),
          '#title_display' => 'before',
          '#markup' => commerce_currency_format(abs($coupon_details['amount_applied']), commerce_default_currency()),
        ];

        // The remove button to remove the gift card from the order.
        $form['horizontal_tabs']['gift_card']['previous_gift_cards_fieldset'][$key]['remove'] = [
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#ajax' => [
            'callback' => '_commerce_sos_horizontal_tabs_ajax_callback',
          ],
          '#coupon_id' => $key,
          '#name' => 'remove-gift-card-' . $key,
          '#element_key' => 'remove-gift-card',
          '#submit' => ['_commerce_sos_horizontal_tabs_submit'],
          '#limit_validation_errors' => [['horizontal_tabs', 'gift_card']],
        ];
      }
    }

    $form['horizontal_tabs']['gift_card']['search_results_fieldset'] = [
      '#type' => 'fieldset',
      '#theme' => 'commerce_sos_gift_card_tables',
      '#attributes' => ['class' => ['element-invisible']],
      '#prefix' => '<div id="sos-gc-search-results-fieldset">',
      '#suffix' => '</div>',
      '#empty_result_text' => t('We couldn\'t find gift cards that match the search criteria.'),
      '#header' => [
        t('GC Code'),
        t('Card Balance'),
        t('Use Amount'),
        t('Operation'),
      ],
      '#table_id' => 'sos-gc-results-table',
    ];

    if (isset($form_state['empty_gift_card_results'])) {
      $form['horizontal_tabs']['gift_card']['search_results_fieldset']['#attributes']['class'] = [];
    }

    if (!empty($form_state['gift_card_results'])) {
      $form['horizontal_tabs']['gift_card']['search_results_fieldset']['#attributes']['class'] = [];
      foreach ($form_state['gift_card_results'] as $key => $code) {
        $form['horizontal_tabs']['gift_card']['search_results_fieldset'][$key]['gc_code'] = [
          '#type' => 'item',
          '#markup' => $code,
        ];

        $card_balance = commerce_gc_giftcard_balance($key);
        $balance_display = commerce_currency_format($card_balance, commerce_default_currency());
        $form['horizontal_tabs']['gift_card']['search_results_fieldset'][$key]['card_balance'] = [
          '#type' => 'item',
          '#markup' => $balance_display,
        ];

        // Calculate the default value to use for the use_amount.
        $use_amount = $balance['amount'] >= $card_balance ? $card_balance : $balance['amount'];

        // Disable the field if the card doesn't have any balance.
        $disabled = $balance == 0 ? TRUE : FALSE;

        $form['horizontal_tabs']['gift_card']['search_results_fieldset'][$key]['use_amount'] = [
          '#type' => 'textfield',
          '#field_prefix' => commerce_currency_get_symbol($currency_code),
          '#default_value' => commerce_currency_amount_to_decimal($use_amount, $currency_code),
          '#disabled' => $disabled,
        ];

        $form['horizontal_tabs']['gift_card']['search_results_fieldset'][$key]['apply'] = [
          '#type' => 'submit',
          '#value' => t('Apply'),
          '#name' => 'apply-gift-card-' . $key,
          '#element_key' => 'apply-gift-card',
          '#coupon_code' => $code,
          '#coupon_id' => $key,
          '#ajax' => [
            'callback' => '_commerce_sos_horizontal_tabs_ajax_callback',
          ],
          '#submit' => ['_commerce_sos_apply_giftcard_submit'],
          '#validate' => ['_commerce_sos_apply_giftcard_validate'],
          '#access' => $disabled ? FALSE : TRUE,
          '#limit_validation_errors' => [['horizontal_tabs', 'gift_card']],
        ];
      }
    }
  }

  if ($form_state['commerce_order']->status == 'checkout_payment') {
    $form['email_customer_receipt'] = [
      '#type' => 'checkbox',
      '#title' => t('Email receipt to customer'),
      '#default_value' => isset($form_state['values']['email_customer_receipt']) ? $form_state['values']['email_customer_receipt'] : TRUE,
    ];

    $form['finish_pay'] = [
      '#type' => 'submit',
      '#value' => t('Finish: Pay Now'),
      '#attributes' => ['class' => ['finish-pay-now', 'blue-primary-button']],
      '#validate' => ['_commerce_sos_finish_pay_validate'],
      '#submit' => ['_commerce_sos_finish_pay_submit'],
      '#suffix' => '<div id="sos-stock-note">' . t('<span>Note: </span>Stock will be adjusted immediately.') . '</div>',
      '#limit_validation_errors' => [['horizontal_tabs', 'gift_card']],
    ];
  }

  return $form;
}

/**
 * Validate function for the gift card search button.
 */
function _commerce_sos_gift_card_search_validate($form, &$form_state) {
  if (empty($form_state['values']['horizontal_tabs']['gift_card']['search'])) {
    form_set_error('horizontal_tabs][gift_card][search', t('Gift card code cannot be empty.'));
  }
}

/**
 * Submit function for the gift card search button.
 */
function _commerce_sos_gift_card_search_submit($form, &$form_state) {
  $form_state['gift_card_results'] = [];
  $query = new EntityFieldQuery();
  $results = $query
    ->entityCondition('entity_type', 'commerce_coupon')
    ->propertyCondition('type', 'giftcard_coupon')
    ->propertyCondition('code', $form_state['values']['horizontal_tabs']['gift_card']['search'], 'CONTAINS')
    ->propertyOrderBy('code')
    ->range(0, 5)
    ->execute();

  if (!empty($results['commerce_coupon'])) {
    $coupons = commerce_coupon_load_multiple(array_keys($results['commerce_coupon']));
    foreach ($coupons as $coupon) {
      $form_state['gift_card_results'][$coupon->coupon_id] = $coupon->code;
    }
  }
  else {
    $form_state['empty_gift_card_results'] = TRUE;
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback to replace the horizontal tabs.
 */
function _commerce_sos_horizontal_tabs_ajax_callback($form, &$form_state) {
  $commands = [];
  $commands[] = ajax_command_remove('div.messages');
  $commands[] = ajax_command_replace('#sos-order-total-summary-view', drupal_render($form['order_total_summary_view']));
  $commands[] = ajax_command_replace('#sos-order-total-summary', drupal_render($form['order_total_summary']));
  $commands[] = ajax_command_replace('#sos-horizontal-tabs-wrapper', drupal_render($form['horizontal_tabs']));
  $commands[] = ajax_command_prepend('#sos-horizontal-tabs-wrapper', theme('status_messages'));
  $commands[] = [
    'command' => 'setActiveTab',
    'selector' => 'sos-horizontal-tabs-wrapper',
    'sub_selector' => 'ul.horizontal-tabs-list',
    'index' => _commerce_sos_get_horizontal_tabs_index($form_state['triggering_element']),
  ];

  return ['#type' => 'ajax', '#commands' => $commands];
}

/**
 * Returns the index of the horizontal tab depending on what was clicked.
 *
 * @param array $triggering_element
 *   The triggering element.
 *
 * @return int
 *   The index of the clicked element.
 */
function _commerce_sos_get_horizontal_tabs_index(array $triggering_element) {
  $horizontal_tabs_array = [
    'payment_terminal' => 1,
    'gift_card' => 2,
  ];

  return $horizontal_tabs_array[$triggering_element['#parents'][1]];
}

/**
 * Validate function for the gift card apply button.
 */
function _commerce_sos_apply_giftcard_validate($form, &$form_state) {
  // If no products in order prevent from going to the payment page.
  if ($form_state['total_product_quantity'] == 0) {
    form_set_error('');
    drupal_set_message(t('There are no products in this order.'), 'error');
    $form_state['rebuild'] = TRUE;
  }

  // Check if stock is still available for all the products in the order.
  // If not, give user a warning that items are not in stock.
  // First make sure all products in the order have stock.
  $products = commerce_sos_order_has_out_of_stock($form_state['commerce_order']);

  if (!empty($products)) {
    form_set_error('');
    drupal_set_message(t('The payment cannot be processed as the following products in the order do not have sufficient stock: @products.', [
      '@products' => implode(', ', $products),
    ]), 'error');
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Submit function for the gift card apply button.
 */
function _commerce_sos_apply_giftcard_submit($form, &$form_state) {
  $amount = $form_state['values']['horizontal_tabs']['gift_card']['search_results_fieldset'][$form_state['triggering_element']['#coupon_id']]['use_amount'];
  $order = commerce_sos_apply_gift_card($form_state['commerce_order'], $form_state['triggering_element']['#coupon_id'], $amount);
  if ($order) {
    unset($form_state['gift_card_results']);
  }

  $form_state['order_updated'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit function for the gift card remove button.
 */
function _commerce_sos_horizontal_tabs_submit($form, &$form_state) {
  $form_state['order_updated'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Validation callback for the payment terminal to check the amount data type
 * and convert it to a proper integer amount on input.
 */
function _commerce_sos_payment_terminal_validate($element, &$form_state) {
  // If a payment method has already been selected.
  if (!empty($form_state['payment_method']) && !empty($form_state['values']['horizontal_tabs']['payment_terminal']['amount'])) {
    if (!is_numeric($form_state['values']['horizontal_tabs']['payment_terminal']['amount'])) {
      form_set_error('horizontal_tabs][payment_terminal][amount', t('You must enter a numeric amount value.'));
    }
    else {
      form_set_value($element['amount'], commerce_currency_decimal_to_amount($form_state['values']['horizontal_tabs']['payment_terminal']['amount'], $form_state['values']['horizontal_tabs']['payment_terminal']['currency_code']), $form_state);
    }
  }
}

/**
 * Validate function for the add payment button.
 */
function _commerce_sos_add_payment_validate($form, &$form_state) {
  // First make sure all products in the order have stock.
  $products = commerce_sos_order_has_out_of_stock($form_state['commerce_order']);

  if (!empty($products)) {
    form_set_error('', t('The payment cannot be processed as the following products in the order do not have sufficient stock: @products.', [
      '@products' => implode(', ', $products),
    ]));
  }
  // Now process the payment.
  elseif ($form_state['payment_method']) {
    $order = $form_state['commerce_order'];
    $balance = commerce_payment_order_balance($form_state['commerce_order']);

    // Make sure we don't have a 0 balance.
    // Make sure amount is not 0.
    if ($balance['amount'] == 0) {
      form_set_error('', t('The payment cannot be processed as the balance for this order is 0.'));
    }
    else {
      // Make sure amount is not 0.
      if ($form_state['values']['horizontal_tabs']['payment_terminal']['amount'] == 0) {
        form_set_error('horizontal_tabs][payment_terminal][amount', t('Amount cannot be 0.'));
      }

      // Make sure amount is not greater than remaining order balance.
      if ($balance && $form_state['values']['horizontal_tabs']['payment_terminal']['amount'] > $balance['amount']) {
        form_set_error('horizontal_tabs][payment_terminal][amount', t('The entered amount is greater than the remaining balance on the order total.'));
      }

      // Find out if the payment details are valid before attempting to process.
      if ($callback = commerce_payment_method_callback($form_state['payment_method'], 'submit_form_validate')) {
        $callback($form_state['payment_method'], $form['horizontal_tabs']['payment_terminal']['payment_details'], $form_state['values']['horizontal_tabs']['payment_terminal']['payment_details'], $order, [
          'horizontal_tabs',
          'payment_terminal',
          'payment_details',
        ]);
      }

      // If the user selected full amount and they didn't enter a full amount,
      // display an error.
      if ($form_state['values']['horizontal_tabs']['payment_terminal']['amount_type'] == 'full_amount') {
        if ($balance && $form_state['values']['horizontal_tabs']['payment_terminal']['amount'] < $balance['amount']) {
          form_set_error('horizontal_tabs][payment_terminal][amount', t('You selected full amount but the amount entered is less than the remaining balance on the order.'));
        }
      }
    }
  }
}

/**
 * Makes a credit card payment when the 'Add Payment' button is clicked.
 */
function _commerce_sos_add_credit_card_payment($form, &$form_state) {
  unset($form_state['out_of_stock_warning_shown']);
  $payment_method = $form_state['payment_method'];
  $order = $form_state['commerce_order'];

  // Delegate submit to the payment method callback.
  if ($callback = commerce_payment_method_callback($payment_method, 'submit_form_submit')) {
    $charge = [
      'amount' => $form_state['values']['horizontal_tabs']['payment_terminal']['amount'],
      'currency_code' => $form_state['values']['horizontal_tabs']['payment_terminal']['currency_code'],
    ];

    $details_form = !empty($form['horizontal_tabs']['payment_terminal']['payment_details']) ? $form['horizontal_tabs']['payment_terminal']['payment_details'] : [];
    $details_values = !empty($form_state['values']['horizontal_tabs']['payment_terminal']['payment_details']) ? $form_state['values']['horizontal_tabs']['payment_terminal']['payment_details'] : [];

    $result = $callback($payment_method, $details_form, $details_values, $order, $charge);

    if ($result === FALSE) {
      $form_state['rebuild'] = TRUE;
    }
    else {
      drupal_set_message(t('@amount_type of @amount successfully processed.', [
        '@amount_type' => $form_state['complete form']['horizontal_tabs']['payment_terminal']['amount_type']['#options'][$form_state['values']['horizontal_tabs']['payment_terminal']['amount_type']],
        '@amount' => commerce_currency_format($form_state['values']['horizontal_tabs']['payment_terminal']['amount'], $form_state['values']['horizontal_tabs']['payment_terminal']['currency_code']),
      ]));
    }
  }
}

/**
 * Validate function for the Pay Now button.
 */
function _commerce_sos_finish_pay_validate($form, &$form_state) {
  // If no products in order prevent from going to the payment page.
  if ($form_state['total_product_quantity'] == 0) {
    form_set_error('');
    drupal_set_message(t('There are no products in this order.'), 'error');
    $form_state['rebuild'] = TRUE;
  }

  // Check if stock is still available for all the products in the order.
  // If not, give user a warning that items are not in stock.
  // First make sure all products in the order have stock.
  $products = commerce_sos_order_has_out_of_stock($form_state['commerce_order']);

  if (!empty($products)) {
    form_set_error('');
    drupal_set_message(t('The payment cannot be processed as the following products in the order do not have sufficient stock: @products.', [
      '@products' => implode(', ', $products),
    ]), 'error');
    $form_state['rebuild'] = TRUE;
  }
  else {
    // If there is still balance to be paid in the order, display an error.
    $balance = commerce_payment_order_balance($form_state['commerce_order']);
    if ($balance['amount'] > 0) {
      form_set_error('');
      drupal_set_message(t('This order still has unpaid balance.'), 'error');
      $form_state['rebuild'] = TRUE;
    }
  }
}

/**
 * Submit function for the Pay Now button.
 */
function _commerce_sos_finish_pay_submit($form, &$form_state) {
  // If send customer receipt is NOT checked, save that in the order object, so
  // that we can add a condition to the confirmation email rule that will only
  // send if this value doesn't exist.
  if ($form_state['values']['email_customer_receipt'] == 0) {
    $order = $form_state['commerce_order'];
    $order->data['email_customer_receipt'] = 0;
    commerce_order_save($order);
  }

  // Complete the checkout, so that all necessary rules will fire.
  commerce_checkout_complete($order);

  drupal_set_message(t('Successfully finished payment for this order.'));

  // Redirect to the view order page.
  drupal_goto('admin/commerce/sos/orders/' . $form_state['commerce_order']->order_id);
}
