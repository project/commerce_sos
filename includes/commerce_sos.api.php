<?php

/**
 * @file
 * Hooks related to commerce_sos module.
 */

/**
 * Allows altering the /cart view and checkout form header content.
 *
 * @param int $order_id
 *   The ID of the commerce order.
 * @param string $content
 *   The html for the header content.
 */
function hook_commerce_sos_order_header_content_alter($order_id, &$content) {
  $order = commerce_order_load($order_id);

  if ($order->status == 'completed') {
    $content = '';
  }
}

/**
 * Allows altering the new username created from a customer's email address.
 *
 * @param string $name
 *   The cleaned up user name derived from the email.
 * @param string $new_unique_name
 *   The new and unique user name.
 */
function hook_commerce_sos_unique_username_alter($name, &$new_unique_name) {
  if ($name = 'something') {
    $new_unique_name = 'something_else';
  }
}

/**
 * Allows altering the html for the bill/ship address shown in the order form.
 *
 * @param array $address_info
 *   The billing or shipping address profile info along with customer details.
 * @param bool $edit_order
 *   True if we're in the SOS order editing page.
 * @param string $output
 *   The html output of the address details.
 */
function hook_commerce_sos_print_address_details_alter(array $address_info, $edit_order, &$output) {
  if ($address_info['profile']->type = 'billing') {
    $output .= '<p>something</p>';
  }
}

/**
 * Allows altering the status of an order before saving it.
 *
 * @param object $order
 *   The commerce order entity.
 * @param string $save_type
 *   The type of saving we're about to do ie. 'void_order'.
 * @param string $status
 *   The status we're going to change the order to.
 */
function hook_commerce_sos_order_save_alter($order, $save_type, &$status) {
  if ($save_type == 'something') {
    $status = 'canceled';
  }
}

/**
 * Allows altering the display path if a line item.
 *
 * @param object $line_item
 *   The commerce order line item entity.
 * @param object $product
 *   The commerce product entity.
 * @param string $display_path
 *   The product display path url.
 */
function hook_commerce_sos_line_item_display_path_alter($line_item, $product, &$display_path) {
  if ($product->type == 'something') {
    $display_path = 'new/url';
  }
}

/**
 * Allows altering the order as shipping line items are deleted.
 *
 * @param string $shipping_service
 *   The name of the shipping service.
 * @param object $rate_line_item
 *   The rate entity for the shipping service.
 * @param object $order
 *   The order entity.
 */
function hook_commerce_sos_delete_shipping_line_items_alter($shipping_service, $rate_line_item, &$order) {
  if ($order->status == 'pending') {
    // Do something.
  }
}

/**
 * Allows modules to act upon the main SOS order form AJAX submission.
 *
 * While this is technically possible already through the use of hook_form_alter,
 * this hook allows other modules to set $form_state['order_updated'] to
 * TRUE to force the form to reload the transaction and recalculate order
 * totals.
 *
 * @param array $triggering_element
 *   The element that triggered the AJAX submission. Available directly in the
 *   $form_state variable, but provided for ease-of-use.
 * @param array $form_state
 *   The Drupal form API form state variable.
 */
function hook_commerce_sos_form_ajax_check_alter($triggering_element, &$form_state) {
}

/**
 * Allows modules to act when the order doesn't have any product line items.
 *
 * @param EntityMetadataWrapper $order_wrapper
 *   The order entity wrapper.
 */
function hook_commerce_sos_empty_products_alter(EntityMetadataWrapper $order_wrapper) {
}

/**
 * Allows modules to alter the display node that references a product.
 *
 * @param int $product_id
 *   The product ID to retrieve the display node for.
 *
 * @param array
 *   The display node details, or 0 if none was found.
 */
function hook_commerce_sos_get_product_display_nid_alter($product_id, array &$display_node) {
}
