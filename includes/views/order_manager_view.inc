<?php

/**
 * @file
 * Order Manager View.
 */

$view = new view();
$view->name = 'commerce_sos_order_manager';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'commerce_order';
$view->human_name = 'Commerce SOS Order Manager';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Order Manager';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer commerce simple order system';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'input_required';
$handler->display->display_options['exposed_form']['options']['text_input_required'] = '';
$handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'order_id' => 'order_id',
  'status' => 'status',
  'mail_1' => 'mail_1',
  'commerce_line_items' => 'commerce_line_items',
  'commerce_order_total' => 'commerce_order_total',
  'changed' => 'changed',
  'nothing' => 'nothing',
  'view_order' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'order_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mail_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => 'views-align-left',
    'separator' => '',
    'empty_column' => 0,
  ),
  'commerce_line_items' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
  'commerce_order_total' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nothing' => array(
    'align' => '',
    'separator' => ' | ',
    'empty_column' => 0,
  ),
  'view_order' => array(
    'align' => 'views-align-center',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = 'Create a new order, or locate and complete a customer\'s cart/order.';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There are no orders that match the search criteria.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Commerce Order: Referenced line items */
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['id'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['table'] = 'field_data_commerce_line_items';
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['field'] = 'commerce_line_items_line_item_id';
/* Field: Commerce Order: Order ID */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'commerce_order';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Cart/Order ID';
/* Field: Commerce Order: Order status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'commerce_order';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['label'] = 'Status';
/* Field: Commerce Order: E-mail */
$handler->display->display_options['fields']['mail_1']['id'] = 'mail_1';
$handler->display->display_options['fields']['mail_1']['table'] = 'commerce_order';
$handler->display->display_options['fields']['mail_1']['field'] = 'mail';
$handler->display->display_options['fields']['mail_1']['label'] = 'Customer';
$handler->display->display_options['fields']['mail_1']['empty'] = 'anonymous';
$handler->display->display_options['fields']['mail_1']['render_as_link'] = 0;
/* Field: COUNT(DISTINCT Commerce Order: Line items) */
$handler->display->display_options['fields']['commerce_line_items']['id'] = 'commerce_line_items';
$handler->display->display_options['fields']['commerce_line_items']['table'] = 'field_data_commerce_line_items';
$handler->display->display_options['fields']['commerce_line_items']['field'] = 'commerce_line_items';
$handler->display->display_options['fields']['commerce_line_items']['group_type'] = 'count_distinct';
$handler->display->display_options['fields']['commerce_line_items']['label'] = '# of Items';
$handler->display->display_options['fields']['commerce_line_items']['separator'] = ', ';
/* Field: Commerce Order: Order total */
$handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['label'] = 'Order Total';
$handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
  'calculation' => FALSE,
);
$handler->display->display_options['fields']['commerce_order_total']['group_column'] = 'entity_id';
/* Field: Commerce Order: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'commerce_order';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Last Modified';
$handler->display->display_options['fields']['changed']['date_format'] = 'raw time ago';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = 'Operation';
/* Field: Commerce Order: Link */
$handler->display->display_options['fields']['view_order']['id'] = 'view_order';
$handler->display->display_options['fields']['view_order']['table'] = 'commerce_order';
$handler->display->display_options['fields']['view_order']['field'] = 'view_order';
$handler->display->display_options['fields']['view_order']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['view_order']['alter']['text'] = 'View';
$handler->display->display_options['fields']['view_order']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['view_order']['alter']['path'] = 'admin/commerce/sos/orders/[order_id]/view';
$handler->display->display_options['fields']['view_order']['alter']['target'] = '_blank';
$handler->display->display_options['fields']['view_order']['text'] = 'View';
/* Sort criterion: Commerce Order: Created date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'commerce_order';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Sort criterion: Commerce Order: Updated date */
$handler->display->display_options['sorts']['changed']['id'] = 'changed';
$handler->display->display_options['sorts']['changed']['table'] = 'commerce_order';
$handler->display->display_options['sorts']['changed']['field'] = 'changed';
$handler->display->display_options['sorts']['changed']['order'] = 'DESC';
/* Filter criterion: Commerce Order: Order ID */
$handler->display->display_options['filters']['order_id']['id'] = 'order_id';
$handler->display->display_options['filters']['order_id']['table'] = 'commerce_order';
$handler->display->display_options['filters']['order_id']['field'] = 'order_id';
$handler->display->display_options['filters']['order_id']['group'] = 1;
$handler->display->display_options['filters']['order_id']['exposed'] = TRUE;
$handler->display->display_options['filters']['order_id']['expose']['operator_id'] = 'order_id_op';
$handler->display->display_options['filters']['order_id']['expose']['label'] = 'Cart/Order ID';
$handler->display->display_options['filters']['order_id']['expose']['operator'] = 'order_id_op';
$handler->display->display_options['filters']['order_id']['expose']['identifier'] = 'order_id';
$handler->display->display_options['filters']['order_id']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
);
/* Filter criterion: Commerce Order: Order total (commerce_order_total:amount) */
$handler->display->display_options['filters']['commerce_order_total_amount']['id'] = 'commerce_order_total_amount';
$handler->display->display_options['filters']['commerce_order_total_amount']['table'] = 'field_data_commerce_order_total';
$handler->display->display_options['filters']['commerce_order_total_amount']['field'] = 'commerce_order_total_amount';
$handler->display->display_options['filters']['commerce_order_total_amount']['group'] = 1;
$handler->display->display_options['filters']['commerce_order_total_amount']['exposed'] = TRUE;
$handler->display->display_options['filters']['commerce_order_total_amount']['expose']['operator_id'] = 'commerce_order_total_amount_op';
$handler->display->display_options['filters']['commerce_order_total_amount']['expose']['label'] = 'Order Total';
$handler->display->display_options['filters']['commerce_order_total_amount']['expose']['description'] = 'The web cart\'s Order Total.';
$handler->display->display_options['filters']['commerce_order_total_amount']['expose']['operator'] = 'commerce_order_total_amount_op';
$handler->display->display_options['filters']['commerce_order_total_amount']['expose']['identifier'] = 'commerce_order_total_amount';
$handler->display->display_options['filters']['commerce_order_total_amount']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
);
/* Filter criterion: Commerce Order: E-mail */
$handler->display->display_options['filters']['mail']['id'] = 'mail';
$handler->display->display_options['filters']['mail']['table'] = 'commerce_order';
$handler->display->display_options['filters']['mail']['field'] = 'mail';
$handler->display->display_options['filters']['mail']['group'] = 1;
$handler->display->display_options['filters']['mail']['exposed'] = TRUE;
$handler->display->display_options['filters']['mail']['expose']['operator_id'] = 'mail_op';
$handler->display->display_options['filters']['mail']['expose']['label'] = 'Customer E-mail';
$handler->display->display_options['filters']['mail']['expose']['description'] = 'Requires a fully qualified email address.';
$handler->display->display_options['filters']['mail']['expose']['operator'] = 'mail_op';
$handler->display->display_options['filters']['mail']['expose']['identifier'] = 'mail';
$handler->display->display_options['filters']['mail']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
);
/* Filter criterion: Commerce Line Item: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['filters']['type']['value'] = array(
  'product' => 'product',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/commerce/sos/orders';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Order Manager';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
