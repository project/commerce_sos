<?php

/**
 * @file
 * Order Messages View.
 */

$view = new view();
$view->name = 'commerce_sos_order_messages';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'message';
$view->human_name = 'Commerce SOS Order Messages';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'mini';
$handler->display->display_options['pager']['options']['items_per_page'] = '5';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'mid' => 'mid',
  'message_render' => 'message_render',
  'name' => 'name',
  'timestamp' => 'timestamp',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'mid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'message_render' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'timestamp' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['empty_table'] = TRUE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No messages have been recorded for this order.';
$handler->display->display_options['empty']['area']['format'] = 'full_html';
/* Relationship: Message: User uid */
$handler->display->display_options['relationships']['user']['id'] = 'user';
$handler->display->display_options['relationships']['user']['table'] = 'message';
$handler->display->display_options['relationships']['user']['field'] = 'user';
$handler->display->display_options['relationships']['user']['required'] = TRUE;
/* Relationship: Message: Type */
$handler->display->display_options['relationships']['type']['id'] = 'type';
$handler->display->display_options['relationships']['type']['table'] = 'message';
$handler->display->display_options['relationships']['type']['field'] = 'type';
$handler->display->display_options['relationships']['type']['required'] = TRUE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['message_commerce_order_target_id']['id'] = 'message_commerce_order_target_id';
$handler->display->display_options['relationships']['message_commerce_order_target_id']['table'] = 'field_data_message_commerce_order';
$handler->display->display_options['relationships']['message_commerce_order_target_id']['field'] = 'message_commerce_order_target_id';
$handler->display->display_options['relationships']['message_commerce_order_target_id']['required'] = TRUE;
/* Field: Message: Render message (Get text) */
$handler->display->display_options['fields']['message_render']['id'] = 'message_render';
$handler->display->display_options['fields']['message_render']['table'] = 'message';
$handler->display->display_options['fields']['message_render']['field'] = 'message_render';
$handler->display->display_options['fields']['message_render']['label'] = 'Description';
$handler->display->display_options['fields']['message_render']['partials'] = 0;
$handler->display->display_options['fields']['message_render']['partials_delta'] = '0';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'user';
$handler->display->display_options['fields']['name']['label'] = 'User';
/* Field: Message: Timestamp */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'message';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['label'] = 'Date';
$handler->display->display_options['fields']['timestamp']['date_format'] = 'short';
$handler->display->display_options['fields']['timestamp']['custom_date_format'] = 'F d, Y';
$handler->display->display_options['fields']['timestamp']['second_date_format'] = 'search_api_facetapi_YEAR';
$handler->display->display_options['fields']['timestamp']['format_date_sql'] = 0;
/* Sort criterion: Message: Timestamp */
$handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['table'] = 'message';
$handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
/* Sort criterion: Message: Message ID */
$handler->display->display_options['sorts']['mid']['id'] = 'mid';
$handler->display->display_options['sorts']['mid']['table'] = 'message';
$handler->display->display_options['sorts']['mid']['field'] = 'mid';
$handler->display->display_options['sorts']['mid']['order'] = 'DESC';
/* Contextual filter: Message: Order (message_commerce_order) */
$handler->display->display_options['arguments']['message_commerce_order_target_id']['id'] = 'message_commerce_order_target_id';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['table'] = 'field_data_message_commerce_order';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['field'] = 'message_commerce_order_target_id';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['message_commerce_order_target_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Message: Message category */
$handler->display->display_options['filters']['type_category']['id'] = 'type_category';
$handler->display->display_options['filters']['type_category']['table'] = 'message';
$handler->display->display_options['filters']['type_category']['field'] = 'type_category';
$handler->display->display_options['filters']['type_category']['value'] = array(
  'commerce_order_message' => 'commerce_order_message',
);
$handler->display->display_options['filters']['type_category']['group'] = 1;
/* Filter criterion: Message: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'message';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'commerce_order_user_comment' => 'commerce_order_user_comment',
);
$handler->display->display_options['filters']['type']['group'] = 1;
