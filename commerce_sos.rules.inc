<?php

/**
 * @file
 * Rules integration for the commerce_sos module.
 */

/**
 * Implements of hook_rules_condition_info().
 */
function commerce_sos_rules_condition_info() {
  return [
    'commerce_sos_send_customer_order_confirmation_receipt' => [
      'label' => t('Send customer order confirmation receipt is checked'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
          'description' => t('The complete order.'),
        ),
      ),
      'group' => 'Commerce SOS',
      'callbacks' => array(
        'execute' => 'commerce_sos_send_customer_order_confirmation_receipt',
      ),
    ],
    'commerce_sos_add_order_item_back_to_stock' => [
      'label' => t('Add order items back to stock'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
          'description' => t('The complete order.'),
        ),
      ),
      'group' => 'Commerce SOS',
      'callbacks' => array(
        'execute' => 'commerce_sos_add_order_items_back_to_stock',
      ),
    ],
  ];
}

/**
 * Determines if we should send the customer an order confirmation email.
 *
 * @param object $order
 *   The order entity
 *
 * @return bool
 *   Returns FALSE if $order->data['email_customer_receipt'] exists & equals 0.
 */
function commerce_sos_send_customer_order_confirmation_receipt($order) {
  if (isset($order->data['email_customer_receipt']) && $order->data['email_customer_receipt'] == 0) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Determines if we should adjust stock on order cancellation.
 *
 * @param object $order
 *   The order entity
 *
 * @return bool
 *   Returns FALSE if $order->data['add_back_to_stock'] exists & equals 0.
 */
function commerce_sos_add_order_items_back_to_stock($order) {
  if (isset($order->data['add_back_to_stock']) && $order->data['add_back_to_stock'] == 0) {
    return FALSE;
  }
  return TRUE;
}
