(function ($) {
  Drupal.behaviors.commerceSOS = {
    attach: function (context, settings) {
      setupProductAutocomplete(context, settings);

      // Prevent the form from getting submitted if
      // the search box is in focus
      $('.commerce-sos-product-search').keydown(function (event) {
        if (event.keyCode == 13 && $('.commerce-sos-product-search').is(':focus')) {
          event.preventDefault();
        }
      });

      // Show ajax throbber while we're searching.
      $('.commerce-sos-product-search').keyup(function() {
        if ($(this).val() != '') {
          $(this).addClass('show-ajax-throbber');
        }
        else {
          $(this).removeClass('show-ajax-throbber');
        }
      });

      // Key Navigator Stuff
      $('commerce-sos-product-autocomplete .commerce-sos-product-display', context).keynavigator({
        activateOn: 'click',
        parentFocusOn: 'mouseover'
      });

      /**
       * Attaches some custom autocomplete functionality to the product search field.
       */
      function setupProductAutocomplete(context, settings) {
        $('.commerce-sos-product-autocomplete', context).each(function () {
          var element = $(this);

          element.once('commerce-sos-autocomplete', function () {
            element.autocomplete({
              source: settings.commerceSOS.productAutoCompleteUrl,
              focus: function (event, ui) {
                event.preventDefault(); // without this: keyboard movements reset the input to ''

                // clear all other rows as selected
                $('.commerce-sos-product-search-item').removeClass('selected');
                ui.item.element.addClass('selected');
              },
              select: function (event, ui) {
                var sku = ui.item.element.find('.btn-add').attr('data-product-sku');

                if (Drupal.settings.commerceSOS.autoCompleteCallback) {
                  Drupal[Drupal.settings.commerceSOS.autoCompleteNamespace][Drupal.settings.commerceSOS.autoCompleteCallback](sku);
                }
                else {
                  addProductSku(sku);
                }
              },
              context: this
            });

            // Override the default UI autocomplete render function.
            element.data('ui-autocomplete')._renderItemData = function (ul, item) {
              var product = $("<li></li>");

              item.element = product;

              product.addClass('commerce-sos-product-search-item')
                .data("ui-autocomplete-item", item)
                .append(item.markup)
                .appendTo(ul)
                .find('.btn-add').click(function (e) {
                e.preventDefault();
                e.stopPropagation(); // prevent product from being added via bubbling

                var sku = $(this).attr('data-product-sku');

                if (Drupal.settings.commerceSOS.autoCompleteCallback) {
                  Drupal[Drupal.settings.commerceSOS.autoCompleteNamespace][Drupal.settings.commerceSOS.autoCompleteCallback](sku);
                }
                else {
                  addProductSku(sku);
                }

                element.data('ui-autocomplete').close();

              });

              // Remove the ajax throbber now that we've got all products.
              $('.commerce-sos-product-search').removeClass('show-ajax-throbber');

              return product;
            };
            // Overide renderMenu to sort the list of autocomplete results
            element.data('ui-autocomplete')._renderMenu = function (ul, items) {
              // Sort the items by title in an inline comparison function
              items.sort(function (a, b) {
                return a.title.localeCompare(b.title)
              });
              $.each(items, function (index, item) {
                element.data('ui-autocomplete')._renderItemData(ul, item);
              });
            }
          });
        });
      }

      /**
       * Populates the product SKU field on the form and triggers its AJAX event.
       */
      function addProductSku(sku) {
        $('.commerce-sos-product-sku-input')
          .val(sku)
          .trigger('blur');
      }

      $('a.sos-show-line-item-attributes').click(function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        $(this).parent('td').parent('tr').next('tr').slideToggle("slow");
        if ($(this).hasClass('edit-clicked')) {
          $(this).removeClass('edit-clicked');
        }
        else {
          $(this).addClass('edit-clicked');
        }
      });

      /**
       * Display the line item attributes only when the 'Edit' link in the order
       * is clicked.
       */
      $('a.sos-show-line-item-attributes').each(function() {
        if (!$(this).hasClass('edit-clicked')) {
          $(this).parent('td').parent('tr').next('tr').hide();
        }
      });

      /**
       * Functionality for vertical tab fields.
       */
      $('fieldset#edit-vertical-tabs-order-status', context).drupalSetSummary(function (context) {
        // If the status has been changed, indicate the original status.
        if ($('#edit-vertical-tabs-order-status-status').val() != $('#edit-vertical-tabs-order-status-original').val()) {
          return Drupal.t('From @title', { '@title' : Drupal.settings.status_titles[$('#edit-vertical-tabs-order-status-original').val()] }) + '<br />' + Drupal.t('To @title', { '@title' : Drupal.settings.status_titles[$('#edit-vertical-tabs-order-status-status').val()] });
        }
        else {
          return Drupal.settings.status_titles[$('#edit-vertical-tabs-order-status-status').val()];
        }
      });

      /**
       * Set the active tab for a vertical/horizontal tab after ajax submit.
       */
      Drupal.ajax.prototype.commands.setActiveTab = function(ajax, response, status) {
        $('#' + response.selector + ' ' + response.sub_selector + ' li:nth-child(' + response.index +') a').click();
      }

      /**
       * Check all the checkboxes when the Select all link is clicked in the
       * add back to stock container.
       */
      $('a#commerce-sos-select-all').click(function(event) {
        event.preventDefault();
        $('#edit-product-modified-container-add-back-to-stock.form-checkboxes').find('input[type="checkbox"]').trigger('click');
      });

      /**
       * Add a class to all checked checkboxes in the add to return form.
       */
      $('#edit-product-modified-container-add-back-to-stock.form-checkboxes input:checkbox').change(function() {
        if($(this).is(':checked')) {
          $(this).parent().addClass('commerce-sos-checkbox-checked');
        }
        else {
          $(this).parent().removeClass('commerce-sos-checkbox-checked');
        }
      });
    }
  }
}(jQuery));
