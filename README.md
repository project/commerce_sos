Using the SOS Order Manager
============================

Viewing Orders
--------------
* Go to /admin/commerce/sos/orders
* You can either search by an order ID, an order total, or by the customer's email.
  * If you'd like to see all orders currently in the system, just click the 'Find Cart' button with all fields empty and it will display all the orders.
* Once you get the list of orders, these are the operations that you can perform from there:
  * **View** - displays a summary view of the order
  * **Process Order** - this link is visible when we have a customer for the order. It takes you to the SOS order edit form.
  * **New Cust.** - this link is visible when we have an anonymous user. It takes you to the SOS create new customer form.
  * **Cust. Lookup** - this link is visible when we have an anonymous user. It takes you to the SOS existing customers page where you can assign a customer for the order.

Creating a New Order
--------------------
* Go to /admin/commerce/sos/orders
* Click on the 'Create New Order' link at the top of the page.
  * **Select Customer** - The first step of creating a new order is selecting a customer for the order. You can either create a new customer or if the email address you enter is already registered to a customer, it will load the details from that customer's profile and populate the fields.
  * **Create the Order** - Once you have entered the customer information, click on the 'Next: Create Order' button. This will take you to the SOS order edit form where you can add products to the order, select the shipping, add discounts, etc.
  * **Order Payment** - Once the order details have been entered, click the 'Finish: Pay Now' button. This will tak you to the payment form where you can use gift cards for payment or pay for the order with multiple credit cards.

Configuring SOS
---------------
* Go to admin/commerce/config/sos
* You can make the following changes to the way the Simple Order System works:
  * **Override Path for Order Creation** - all 'New Order' links will now redirect to the SOS Create New Order form.
  * **Override Path for Order Editing** - all new 'Edit Order' links will now redirect to the SOS Edit Order form.
  * **Billing/Shipping Address Profile** - allows you to select which billing and shipping profile types to use.
  * **Inject Order ID** - if selected, this will display the order ID atop the /cart and /checkout pages.
  * **Product Types In Product Search** - allows you to select which product types to be part of the product search.
  * **Select Image fields** - allows you to select the image field to be used in product searches and order details.
  * **Select Credit Card Payment Service** - allows you to select which payment service to use for making credit card transactions through the SOS payment form.
  * **Fields in Advanced Tab** - allows you to select which fields should be added in the 'Advanced' vertical tab of the order edit form.
  * **Advanced Settings** - configure the number of results to show in the product search and whether to use a search API index for the product search.
