<?php

/**
 * @file
 * Theme callbacks for commerce_sos.
 */

/**
 * Implements template_preprocess_commerce_sos_product_result().
 *
 * Adds additional variables to the template.
 */
function commerce_sos_preprocess_commerce_sos_product_result(&$variables) {
  $product = $variables['product'];

  $variables['image'] = NULL;
  $stock = NULL;

  if (module_exists('commerce_stock') && $stock_field = field_get_items('commerce_product', $product, 'commerce_stock')) {
    $stock = !empty($stock_field[0]['value']) ? (int) $stock_field[0]['value'] : 0;
  }

  $price = commerce_product_calculate_sell_price($product);
  $variables['sell_price'] = commerce_currency_format($price['amount'], $price['currency_code']);

  // Check for an image.
  if ($image = commerce_sos_product_thumbnail($product)) {
    $variables['image'] = $image;
  }

  if ($display_nid = commerce_sos_get_product_display_nid($product->product_id)['nid']) {
    $variables['product_display'] = 'node/' . $display_nid;
  }
  else {
    $variables['product_display'] = NULL;
  }

  $variables['stock'] = $stock !== NULL ? t('@count in stock', ['@count' => $stock]) : NULL;
}
